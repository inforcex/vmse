<?php

namespace App\Listeners;

use Twilio\Rest\Client;
use App\Notifications\NewVisitorEmailNotification;
use App\Events\NewVisitor;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\InformVisitorEmailNotification;

class NewVisitorListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  NewListener  $event
     * @return void
     */
    public function handle(NewVisitor $event)
    {
       $event->employee->notify(new NewVisitorEmailNotification());
       $event->visitor->notify(new InformVisitorEmailNotification());
       $client = new Client(config('twilio.sid'), config('twilio.token'));
       $client->messages->create($event->employee->phone , ["from" => config('twilio.number') , "body" => "You have a new Visitor"]);
    }
}
