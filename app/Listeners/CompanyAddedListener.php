<?php

namespace App\Listeners;

use App\Events\CompanyAdded;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\CompanyAddedEmailNotification;

class CompanyAddedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  CompanyAdded  $event
     * @return void
     */
    public function handle(CompanyAdded $event)
    {
        $event->user->notify(new CompanyAddedEmailNotification($event->user));
    }
}
