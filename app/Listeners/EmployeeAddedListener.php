<?php

namespace App\Listeners;


use Twilio\Rest\Client;
use App\Events\EmployeeAdded;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use App\Notifications\EmployeeAddedEmailNotification;

class EmployeeAddedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  EmployeAdded  $event
     * @return void
     */
    public function handle(EmployeeAdded $event)
    {
        $event->user->notify(new EmployeeAddedEmailNotification($event->password));
        $client = new Client(config('twilio.sid'), config('twilio.token'));
        $client->messages->create($event->user->phone , ["from" => config('twilio.number') , "body" => "Your account has been created at Venvoy!"]);
    }
}
