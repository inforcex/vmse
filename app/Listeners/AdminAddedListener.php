<?php

namespace App\Listeners;

use Twilio\Rest\Client;
use Twilio\Exceptions\RestException;
use App\Notifications\AdminAddedEmailNotification;
use App\Events\AdminAdded;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;

class AdminAddedListener
{
    /**
     * Create the event listener.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    /**
     * Handle the event.
     *
     * @param  AdminAdded  $event
     * @return void
     */
    public function handle(AdminAdded $event)
    {
        $event->user->notify(new AdminAddedEmailNotification($event->password));
        $client = new Client(config('twilio.sid'), config('twilio.token'));
        $client->messages->create($event->user->phone , ["from" => config('twilio.number') , "body" => "Your account has been created!"]);
    }
}
