<?php

namespace App;

use App\Traits\HasAvatar;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;
use HipsterJazzbo\Landlord\BelongsToTenants;
use Illuminate\Database\Eloquent\SoftDeletes;

class Visitor extends Model
{
    use Notifiable, BelongsToTenants, SoftDeletes, HasAvatar;

    protected $fillable = [ 
        'first_name',
        'last_name',
        'middle_name',
        'email',
        'phone',
        'national_id',
        'passport_no',
        'gender',
        'company',
        'dob',
        'nationality',
        'visitor_type',
        'status',
        'visit_date',
        'visit_time',
        'departure',
        'department_id',
        'reason',
        'emirate_vehicle',
        'plate_number',
        'front',
        'avatar_id',
        'photo',
        'user_id',
        'tenant_id'
    ];

    protected $dates = ['deleted_at' , 'dob' , 'visit_date'];
    
    public function user()
    {
    	return $this->belongsTo('App\User');
    }

    public function visitorType()
    {
        return $this->hasOne('App\VisitorType' , 'visitor_type');
    }

    public function vehicle()
    {
        return $this->hasOne('App\Vehicle');
    }

    public function fullName()
    {
    	if(!$this->middle_name)
    	{
    		return $this->first_name . " " . $this->last_name;
    	}

    	return $this->first_name . " " . $this->middle_name . " " . $this->last_name; 
    }
}
