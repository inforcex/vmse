<?php

namespace App\Repositories\Eloquent;

use App\Department;
use App\Repositories\Contracts\DepartmentRepository;
use App\Repositories\RepositoryAbstract;



class EloquentDepartmentRepository extends RepositoryAbstract implements DepartmentRepository
{
    public function entity()
    {
        return Department::class;
    }
}