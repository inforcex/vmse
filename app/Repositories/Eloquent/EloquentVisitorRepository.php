<?php

namespace App\Repositories\Eloquent;

use App\Visitor;
use App\Repositories\Contracts\VisitorRepository;
use App\Repositories\RepositoryAbstract;


class EloquentVisitorRepository extends RepositoryAbstract implements VisitorRepository
{
    public function entity()
    {
        return Visitor::class;
    }
}