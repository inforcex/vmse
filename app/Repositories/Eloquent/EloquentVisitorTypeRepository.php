<?php

namespace App\Repositories\Eloquent;

use App\VisitorType;
use App\Repositories\Contracts\VisitorTypeRepository;
use App\Repositories\RepositoryAbstract;


class EloquentVisitorTypeRepository extends RepositoryAbstract implements VisitorTypeRepository
{
    public function entity()
    {
        return VisitorType::class;
    }
}