<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use HipsterJazzbo\Landlord\BelongsToTenants;

class VisitorType extends Model
{
    use BelongsToTenants;
    
    protected $fillable = ['name' , 'tenant_id'];
}
