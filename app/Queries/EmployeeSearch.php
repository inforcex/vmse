<?php

namespace App\Queries;

use App\User as Employee;

class EmployeeSearch
{
    protected $term;
    protected $tenantId;
    protected $department;

    public function __construct($term , $tenantId , $department)
    {
        $this->term = $term;
        $this->tenantId = $tenantId;
        $this->department = $department;
    }

    public function __invoke()
    {
        $searchQuery = Employee::select(['id' , 'name'])->where('name' , 'like' , '%' . $this->term . '%')->whereHas('roles' , function($query){
            $query->where('role_id' , 3);
        })->where('tenant_id' , $this->tenantId);

        if($this->department)
        {
            $searchQuery->where('department_id' , $this->department);
        }

        return $searchQuery->get();
    }
}