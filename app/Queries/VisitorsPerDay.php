<?php

namespace App\Queries;
use DB;
class VisitorsPerDay
{
    public function __invoke()
    {
        return DB::table('visitors')->select(DB::raw('count(*) as visitors , DATE(visit_date) as added'))
                             ->groupBy(DB::raw('DATE(visit_date)'))
                             ->latest()
                             ->limit(20)
                             ->get();
    }
}