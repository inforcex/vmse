<?php

namespace App\Queries;

use App\User;
use App\User as Admin;
use App\User as Employee;
use App\Visitor;
use Carbon\Carbon;
use App\Department;


class Counters
{
    protected $dt;

    public function __construct()
    {
        $this->dt = Carbon::now();
    }
    public function signedInVisitors()
    {
        return Visitor::where('visit_date' , $this->dt->format('Y-m-d'))
                        ->where('visit_time' , '<=' , $this->dt->format('H:i:s'))
                        ->where('departure' , '>=' , $this->dt->format('H:i:s'))
                        ->count();
    }

    public function signedOutVisitors()
    {
        return Visitor::where('visit_date' , $this->dt->format('Y-m-d'))
                        ->where('departure' , '<' , $this->dt->format('H:i:s'))
                        ->count();
    }

    public function expectedSignInVisitors()
    {
        return Visitor::where('visit_date' , $this->dt->format('Y-m-d'))
                        ->where('visit_time' , '>' , $this->dt->format('H:i:s'))
                        ->count();
    }

}