<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Zizaco\Entrust\Traits\EntrustUserTrait;
use HipsterJazzbo\Landlord\BelongsToTenants;
use App\Traits\HasAvatar;

class User extends Authenticatable
{
    use EntrustUserTrait , Notifiable , BelongsToTenants  , HasAvatar;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password', 'phone' , 'avatar_id' , 'welcome_message' , 'department_id'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function visitors()
    {
        return $this->hasMany('App\Visitor');
    }

    public function department()
    {
        return $this->belongsTo('App\Department');
    }
}
