<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use App\Repositories\Contracts\{
    DepartmentRepository,
    VisitorRepository,
    UserRepository,
    VisitorTypeRepository
};
use App\Repositories\Eloquent\{
    EloquentDepartmentRepository,
    EloquentUserRepository,
    EloquentVisitorRepository,
    EloquentVisitorTypeRepository
};

class RepositoryServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap the application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(DepartmentRepository::class , EloquentDepartmentRepository::class);
        $this->app->bind(VisitorRepository::class , EloquentVisitorRepository::class);
        $this->app->bind(UserRepository::class , EloquentUserRepository::class);
        $this->app->bind(VisitorTypeRepository::class , EloquentVisitorTypeRepository::class);
    }

    /**
     * Register the application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
