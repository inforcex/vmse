<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use HipsterJazzbo\Landlord\BelongsToTenants;

class Department extends Model
{
    use BelongsToTenants;

    protected $fillable = ['name' , 'slug' , 'location' , 'tenant_id'];

    public function users()
    {
        return $this->hasMany('App\User');
    }
}
