<?php

namespace App\Traits;

trait HasAvatar
{
    public function avatar()
    {
        return $this->hasOne('App\Image' , 'id' , 'avatar_id');
    }

    public function avatarPath()
    {
        if(!$this->avatar_id)
        {
            return null;
        }

        return $this->avatar->path();
    }
}