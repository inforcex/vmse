<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class CreateVisitorRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'first_name' => 'required|string|max:255',
            'middle_name' => '',
            'last_name' => 'string|max:255',
            'email' => 'required|string|email|max:255',
            'phone' => 'required',
            'user_id' => 'required',
            'tenant_id' => 'integer',
            'department_id' => '',
            'national_id' => 'required|string|max:255',
            'gender' => 'required',
            'nationality' => '',
            'emirate_vehicle' => '',
            'plate_number' => '',
            'visitor_type' => 'required',
            'visit_date' => 'required',
            'visit_time' => 'required',
            'front' => 'required',
            'reason' => 'required|string|max:255',
            'dob' => 'nullable|date',
            'passport_no' => ''

        ];
    }

    public function messages()
    {
        return[
            'user_id.required' => 'Please select a host.'
        ];
    }
}
