<?php

namespace App\Http\Middleware;

use Closure;
use HipsterJazzbo\Landlord\Facades\Landlord;
use App\Department;

class TenantMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if(auth()->check())
        {
            $tenantId = auth()->user()->tenant_id;
            Landlord::addTenant('tenant_id', $tenantId);
            Landlord::applyTenantScopes(auth()->user());
            Landlord::applyTenantScopes(new Department());
        }
        return $next($request);
    }
}
