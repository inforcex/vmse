<?php

namespace App\Http\Controllers;

use App\User;
use App\Events\AdminAdded;
use Illuminate\Http\Request;
use Twilio\Exceptions\RestException;
use App\Http\Requests\CreateAdminRequest;

class AdminsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('admins.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admins.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Http\Requests\CreateAdminRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateAdminRequest $request)
    {
        
        $randomPassword = str_random(6);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => bcrypt($randomPassword)
        ]);

        $user->roles()->attach([2,3]); //admin is also an employee

        try {
            event(new AdminAdded($user , $randomPassword));
        } catch (RestException $e) {
            return redirect()->back()->with([
                                'message' => $e->getMessage(),
                                'alert-type' => 'error'
                            ]);
        }
        
        return redirect()->route('admins.index')
                         ->with([
                                'message' => 'Successfully created admin.',
                                'alert-type' => 'success'
                            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('admins.edit' , compact('user'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $admin = User::find($id);
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . $admin->id,
            'phone' => 'required|string|unique:users,phone,' . $admin->id,

        ]);
        
        $admin->name = $request->name;
        $admin->email = $request->email;
        $admin->phone = $request->phone;
        $admin->save();
        return redirect()->route('admins.index')->with(['message' => 'Successfully Updated!' , 'alert-type' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('admins.index')->with(['message' => 'Successfully Deleted!' , 'alert-type' => 'success']);
    }
}
