<?php

namespace App\Http\Controllers\Employees;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Queries\EmployeeSearch;

class EmployeeSearchController extends Controller
{
    public function show(Request $request)
    {
        $employees = new EmployeeSearch($request->q , $request->tenant , $request->department);
        return response()->json($employees());
    }
}
