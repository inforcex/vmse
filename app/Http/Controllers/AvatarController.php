<?php

namespace App\Http\Controllers;

use App\Image;
use Illuminate\Http\Request;
use Intervention\Image\ImageManager;

class AvatarController extends Controller
{
	protected $imageManager;

    public function __construct(ImageManager $imageManager)
    {
    	$this->imageManager = $imageManager;
    }

    public function store(Request $request)
    {
    	$request->validate([
    		'avatar' => 'required|image|max:1024'
    	]);

    	$processedImage = $this->imageManager->make($request->file('avatar')->getPathName())->fit(128,128 ,function($c){
    		$c->aspectRatio();
    	})->encode('png')
		  ->save(config('image.path.absolute') . $path = '/' . uniqid(true) . '.png');

		$image = Image::create([
			'path' => $path
		]);

    	return response([

    		'data' => [
    			'id' => $image->id,
    			'path' => $image->path()
    		]

    	] , 200);
    }

    public function updateAvatar(Request $request)
    {
    	$request->user()->update($request->only('avatar_id'));
    	return redirect()->back()->with([
                                'message' => 'Successfully updated your Avatar',
                                'alert-type' => 'success'
                            ]);;
    }
}
