<?php

namespace App\Http\Controllers;

use DB;
use Illuminate\Http\Request;
use App\Queries\VisitorsPerDay;

class ReportsController extends Controller
{
   public function index()
   {
        $visitorsPerDay = new VisitorsPerDay();
        $labels = $visitorsPerDay()->pluck('added');
        $values = $visitorsPerDay()->pluck('visitors');
       return view('reports.index' , ['labels' => $labels , 'visitors' => $values]);
   }

   public function visitorsByDate(Request $request)
   {
    $visitors =  DB::table('visitors')->select(DB::raw('count(*) as visitors , DATE(visit_date) as added'))
                ->whereBetween('visit_date' , [$request->start , $request->end])
                ->groupBy(DB::raw('DATE(visit_date)'))
                ->latest()
                ->limit(20)
                ->get();
    $labels = $visitors->pluck('added');
    $values = $visitors->pluck('visitors');
    return response()->json(['values' => $values , 'labels' => $labels]);
   }
}
