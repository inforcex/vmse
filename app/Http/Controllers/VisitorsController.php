<?php

namespace App\Http\Controllers;

use App\User;
use App\Visitor;
use App\Events\NewVisitor;
use App\Http\Requests\CreateVisitorRequest;
use Illuminate\Http\Request;
use App\Department;
use App\Repositories\Contracts\{
    DepartmentRepository,
    VisitorTypeRepository
};

class VisitorsController extends Controller
{
    protected $departments;
    protected $visitorTypes;

    public function __construct(DepartmentRepository  $departments , VisitorTypeRepository $visitorTypes)
    {
        $this->middleware('auth')->except('front','store');
        $this->departments = $departments;
        $this->visitorTypes = $visitorTypes;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('visitors.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('visitors.create' , ['departments' => $this->departments->all() , 'visitorTypes' => $this->visitorTypes->all()]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateVisitorRequest $request)
    {
        $visitorData = $request->validated();
        $visitor = Visitor::create($visitorData);
        $employee = User::find($request->user_id);
        try {
            event(new NewVisitor($employee , $visitor));
        } catch (RestException $e) {
            if($request->ajax())
            {
                return response()->json([
                    'message' => $e->getMessage(),
                    'type' => 'error'
                ]);
            }

            return redirect()->back()->with([
                                'message' => $e->getMessage(),
                                'alert-type' => 'error'
                            ]);
        }
        if($request->ajax())
        {
            return response()->json([
                'message' => 'Host has been informed',
                'type' => 'success'
            ]);
        }
        return redirect()->route('visitors.index')->with([
            'message' => 'Host has been informed',
            'alert-type' => 'success'
        ]);

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function show(Visitor $visitor)
    {
        return view('visitors.show' , compact('visitor'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function edit(Visitor $visitor)
    {
        return view('visitors.edit' , ['departments' => $this->departments->all() , 'visitorTypes' => $this->visitorTypes->all() , 'visitor' => $visitor]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function update(CreateVisitorRequest $request, Visitor $visitor)
    {
        $visitor->update($request->validated());
        return redirect()->route('visitors.index')->with([
            'message' => 'Information Updated.',
            'alert-type' => 'success'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Visitor  $visitor
     * @return \Illuminate\Http\Response
     */
    public function destroy(Visitor $visitor)
    {
        $visitor->delete();
        return redirect()->route('visitors.index')->with(['message' => 'Successfully Deleted!' , 'alert-type' => 'success']);
    }

}
