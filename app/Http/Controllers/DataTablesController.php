<?php

namespace App\Http\Controllers;

use App\User;
use App\Visitor;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Yajra\Datatables\Datatables;
use App\Department;
use Illuminate\Support\Facades\Lang;

class DataTablesController extends Controller
{
    public function admins(Request $request)
    {
    	$users = User::select(['id' , 'name', 'email', 'created_at'])->whereHas('roles' , function($query){
            $query->where('role_id' , 2);
        });
    	
    	return Datatables::of($users)
    					->addColumn('action' , function($user){
                            $editUrl = route('admins.edit' , ['id' => $user->id]);
                            $delete = $this->getDeleteHTML('admins' , $user->id);
    						return  "<a href='$editUrl' class='btn btn-primary'>" . Lang::get('buttons.edit')  . "</a>" . $delete;
    					})->editColumn('created_at' , function($user){
                            return $user->created_at ? with(new Carbon($user->created_at))->diffForHumans() : '';
                        })->make(true);
    }

    public function employees(Request $request)
    {
    	$users = User::select(['id' , 'name', 'email', 'created_at'])->whereHas('roles' , function($query){
            $query->where('role_id' , 3);
        });

    	return Datatables::of($users)
    					->addColumn('action' , function($user){
                            $editUrl = route('employees.edit' , ['id' => $user->id]);
                            $delete = $this->getDeleteHTML('employees' , $user->id);
    						return  "<a href='$editUrl' class='btn btn-primary'>" . Lang::get('buttons.edit')  . "</a>" . $delete;
    					})->editColumn('created_at' , function($user){
                            return $user->created_at ? with(new Carbon($user->created_at))->diffForHumans() : '';
                        })->make(true);
    }

    public function departments(Request $request)
    {
        $departments = Department::select(['id' , 'name']);
        return Datatables::of($departments)
                         ->addColumn('action' , function($department){
                            $editUrl = route('departments.edit' , ['id' => $department->id]);
                            $delete = $this->getDeleteHTML('departments' , $department->id);
    						return  "<a href='$editUrl' class='btn btn-primary'>" . Lang::get('buttons.edit')  . "</a>" . $delete;
                         })->make(true);
    }

    public function visitors(Request $request)
    {
        $visitors = Visitor::select(['id' , 'first_name' , 'middle_name' , 'last_name' , 'status', 'email' , 'visit_date' , 'visit_time' , 'departure']);
        return Datatables::of($visitors)
                        ->addColumn('action' , function($visitor){
                            $changeStatusHTML = $this->getStatusHTML($visitor->id , $visitor->status);
                            $leaveHTML = $this->getLeaveHTML($visitor->id , $visitor->departure);
                            if(auth()->user()->hasRole('super_admin'))
                            {
                                $editUrl = route('visitors.edit' , ['id' => $visitor->id]);
                                return  $leaveHTML . '<a href="'. $editUrl .'" class="btn btn-primary">' . Lang::get('buttons.edit')  .'</a>' .  $changeStatusHTML;
                            }
                            return $leaveHTML . $changeStatusHTML;
                        })->editColumn('first_name' , function($visitor){
                            return $visitor->fullName();
                        })->editColumn('visit_date' , function($visitor){
                            return $visitor->visit_date ? with(new Carbon($visitor->visit_date))->format('y-m-d') : '';
                        })->editColumn('status' , function($visitor){
                            return title_case($visitor->status);
                        })->editColumn('departure' , function($visitor){
                            return is_null($visitor->departure) ? '-' : $visitor->departure;
                        })->make(true);
    }

    public function myVisitors(Request $request)
    {
        $visitors = Visitor::select(['id' , 'first_name' , 'middle_name' , 'last_name' , 'status', 'email' , 'created_at'])->where('user_id' , auth()->user()->id);
        return Datatables::of($visitors)
                        ->addColumn('action' , function($visitor){
                            $editUrl = route('visitors.edit' , ['id' => $visitor->id]);
                            $changeStatusHTML = $this->getStatusHTML($visitor->id);
                            $delete = $this->getDeleteHTML('visitors' , $visitor->id);
                            return  '<a href="'. $editUrl .'" class="btn btn-primary">Edit</a>' . $changeStatusHTML . $delete;
                        })->editColumn('first_name' , function($visitor){
                            return $visitor->fullName();
                        })->editColumn('created_at' , function($visitor){
                            return $visitor->created_at ? with(new Carbon($visitor->created_at))->diffForHumans() : '';
                        })->editColumn('status' , function($visitor){
                            return title_case($visitor->status);
                        })->make(true);
    }


    protected function getDeleteHTML($entity , $id)
    {
        $url = route($entity.'.destroy' , ['id' => $id]);
        $html =  "<form method='POST' action='$url' class='delete-form'>";
        $html .= "<input type='hidden' name='_token' value='" . session('_token') . "'>";
        $html .= "<input type='hidden' name='_method' value='DELETE'>";
        $html .= "<button type='submit' class='btn btn-danger delete-submit'>" . Lang::get('buttons.delete') . "</button>";
        $html .= "</form>";
        return $html;
    }

    protected function getStatusHTML($id , $status)
    {
        return "<button data-visitor='$id' data-status='$status' class='btn btn-success waves-effect waves-classic change-status-btn' data-target='#visitorStatusModal' data-toggle='modal'>" . Lang::get('buttons.status') . "</button>";
    }
    protected function getLeaveHTML($id , $departure)
    {
        if($departure)
        {
            return "<button type='button' data-visitor='$id' class='btn btn-warning waves-effect waves-classic leave-btn' disabled><span class='ladda-label'>" . Lang::get('buttons.leave') . "</span></button>";
        }
        return "<button type='button' data-visitor='$id' class='btn btn-warning waves-effect waves-classic leave-btn'><span class='ladda-label'>" . Lang::get('buttons.leave') . "</span></button>";
    }
}
