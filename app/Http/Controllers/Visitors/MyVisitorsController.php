<?php

namespace App\Http\Controllers\Visitors;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class MyVisitorsController extends Controller
{
    public function index(Request $request)
    {
        return view('visitors.my-visitors');
    }
}
