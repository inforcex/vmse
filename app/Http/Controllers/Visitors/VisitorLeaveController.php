<?php

namespace App\Http\Controllers\Visitors;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\VisitorRepository;

class VisitorLeaveController extends Controller
{
    protected $visitors;

    public function __construct(VisitorRepository $visitors)
    {
        $this->visitors = $visitors;
    }

    public function create(Request $request)
    {
        $dt = \Carbon\Carbon::createFromFormat('H:i:s' , $request->departure);
        $departure = $dt->toTimeString();
        $visitor = $this->visitors->update($request->id , ['departure' => $departure]);
        return response()->json(['visitor' => $visitor]);
    }
}
