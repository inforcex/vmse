<?php

namespace App\Http\Controllers\Visitors;

use App\Visitor;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\VisitorRepository;

class VisitorStatusController extends Controller
{
    protected $visitors;

    public function __construct(VisitorRepository $visitors)
    {
        $this->visitors = $visitors;
    }
    
    public function update(Request $request)
    {
        $this->visitors->update($request->get('visitorId') , ['status' => $request->get('status')]);
        return response()->json('success' , 200);
    }
}
