<?php

namespace App\Http\Controllers\Visitors;

use App\User;
use App\Department;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Repositories\Contracts\UserRepository as CompanyRepository;
use App\Repositories\Contracts\DepartmentRepository;
use App\Repositories\Contracts\VisitorTypeRepository;

class VisitorFrontController extends Controller
{
    protected $departments;
    protected $companies;
    protected $visitorTypes;

    public function __construct(CompanyRepository $companies , DepartmentRepository  $departments , VisitorTypeRepository $visitorTypes)
    {
        $this->departments = $departments;
        $this->companies = $companies;
        $this->visitorTypes = $visitorTypes;
    }

    public function create(Request $request , $identifier)
    {
        $company = $this->companies->findWhereFirst('identifier' , $identifier);
        $departments = $this->departments->findWhere('tenant_id' , $company->tenant_id);
        $visitorTypes = $this->visitorTypes->all();
        if($request->has('lang') && $request->filled('lang'))
        {
            return view('visitors.front.create-' . $request->lang , ['company' => $company , 'departments' => $departments , 'visitorTypes' => $visitorTypes]);
        }
        return view('visitors.front.create-en' , ['company' => $company , 'departments' => $departments , 'visitorTypes' => $visitorTypes]);
    }
}
