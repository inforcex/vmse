<?php

namespace App\Http\Controllers;

use App\Department;
use Illuminate\Http\Request;
use App\Repositories\Contracts\DepartmentRepository;

class DepartmentsController extends Controller
{
    protected $departments;

    public function __construct(DepartmentRepository $departments)
    {
        $this->departments = $departments;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('departments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('departments.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validated = $request->validate([
            'name' => 'required|string|max:255'
        ]);
        $this->departments->create($validated);
        return redirect()->route('departments.index')
                        ->with([
                            'message' => 'Successfully created new Department.',
                            'alert-type' => 'success'
                        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function show(Department $department)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function edit(Department $department)
    {
        return view('departments.edit' , ['department' => $department]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Department $department)
    {
        $validated = $request->validate([
            'name' => 'required|string|max:255'
        ]);
        $this->departments->update($department->id , $validated);
        return redirect()->route('departments.index')
                        ->with([
                            'message' => 'Successfully Updated',
                            'alert-type' => 'success'
                        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Department  $department
     * @return \Illuminate\Http\Response
     */
    public function destroy(Department $department)
    {
        $this->departments->delete($department->id);
        return redirect()->route('departments.index')->with(['message' => 'Successfully Deleted!' , 'alert-type' => 'success']);
    }
}
