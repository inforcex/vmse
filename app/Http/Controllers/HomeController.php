<?php

namespace App\Http\Controllers;

use App\Queries\Counters;
use Illuminate\Http\Request;
use App\User;
use App\User as Admin;
use App\User as Employee;
use App\Visitor;
use Carbon\Carbon;
use App\Department;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $counters = new Counters();
        $data = [
            'employees' => $this->employeeCount(),
            'admins' => $this->adminsCount(),
            'visitors' => $this->visitorsCount(),
            'tenant' => $this->getTenantUser(),
            'signedInVisitors' => $counters->signedInVisitors(),
            'signedOutVisitors' => $counters->signedOutVisitors(),
            'expectedVisitors' => $counters->expectedSignInVisitors(),
            'todaysVisitors' => $this->todaysVistiors(),
            'thisMonthVisitors' => $this->thisMonthVisitors(),
            'processingVisitors' => $this->processingVisits(),
            'acceptedVisitors' => $this->acceptedVisits(),
            'rejectedVisitors' => $this->rejectedVisits(),
            'departments' => $this->departmentsCount(),
        ];
        if(auth()->user()->hasRole('employee'))
        {
            $data['employeeTodayVisitors'] = $this->employeesVisitors();
            $data['employeeProcessingVisitors'] = $this->employeeProcessingVisitors();
            $data['employeeAcceptedVisitors'] = $this->employeeAcceptedVisitors();
            $data['employeeRejectedVisitors'] = $this->employeeRejectedVisitors();
            $data['employeeAllVisitors'] = $this->employeeAllVisitors();
        }
        return view('home' , $data);
    }

    protected function employeeCount()
    {
       return Employee::whereHas('roles' , function($query){
            $query->where('role_id' , 3);
        })->count();
    }

    protected function adminsCount()
    {
        return Admin::whereHas('roles' , function($query){
            $query->where('role_id' , 2);
        })->count();
    }

    protected function visitorsCount()
    {
        return Visitor::count();
    }

    protected function getTenantUser()
    {
        $user = auth()->user();
        if(is_null($user->tenant_id))
        {
            return $user;
        }
        $tenant = User::find($user->tenant_id);
        return $tenant;
    }

    protected function todaysVistiors()
    {
        $dt = Carbon::now();
        return Visitor::whereRaw('DAY(created_at) = ?' , $dt->day)->count();
    }

    protected function thisMonthVisitors()
    {
        $dt = Carbon::now();
        $count = Visitor::whereRaw('MONTH(created_at) = ?' , $dt->month)->count();
        return $count;
    }

    protected function processingVisits()
    {
        return Visitor::where('status' , 'processing')->count();
    }

    protected function acceptedVisits()
    {
        return Visitor::where('status' , 'accepted')->count();
    }

    protected function rejectedVisits()
    {
        return Visitor::where('status' , 'rejected')->count();
    }

    protected function employeesVisitors()
    {
        $dt = Carbon::now();
        return Visitor::where('user_id' , auth()->user()->id)->whereRaw(' DAY(created_at) = ?' , $dt->day)->count();
    }

    protected function employeeAllVisitors()
    {
        return Visitor::where('user_id' , auth()->user()->id)->count();
    }

    protected function employeeProcessingVisitors()
    {
        return Visitor::where(['user_id' => auth()->user()->id , 'status' => 'processing'])->count();
    }

    protected function employeeAcceptedVisitors()
    {
        return Visitor::where(['user_id' => auth()->user()->id , 'status' => 'accepted'])->count();
    }

    protected function employeeRejectedVisitors()
    {
        return Visitor::where(['user_id' => auth()->user()->id , 'status' => 'rejected'])->count();
    }

    protected function departmentsCount()
    {
        return Department::count();
    }
}
