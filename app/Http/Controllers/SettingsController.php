<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SettingsController extends Controller
{
    public function index(Request $request)
    {
    	$user = auth()->user();
    	return view('settings.index' , compact('user'));
    }

    public function updateWelcomeMessage(Request $request)
    {
    	$validated = $request->validate([
    		'welcome_message' => 'required|string|max:250'
    	]);

    	$request->user()->update($request->only('welcome_message'));
    	return redirect()->back()->with([
                                'message' => 'Successfully updated Welcome Message',
                                'alert-type' => 'success'
                            ]);
    }
}
