<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use App\Events\EmployeeAdded;
use Twilio\Exceptions\RestException;
use App\Http\Requests\CreateEmployeeRequest;
use App\Repositories\Contracts\DepartmentRepository;
use App\Department;

class EmployeesController extends Controller
{
    protected $department;

    public function __construct(DepartmentRepository $department)
    {
        $this->department = $department;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('employees.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $departments = Department::all();
        return view('employees.create' , compact('departments'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateEmployeeRequest $request)
    {
        $randomPassword = str_random(6);

        $user = User::create([
            'name' => $request->name,
            'email' => $request->email,
            'phone' => $request->phone,
            'password' => bcrypt($randomPassword),
            'department_id' => $request->department
        ]);

        $user->roles()->attach(3); 
        try {
            event(new EmployeeAdded($user , $randomPassword));
        } catch (RestException $e) {
            return redirect()->back()->with([
                    'message' => $e->getMessage(),
                    'alert-type' => 'error'
                ]);
        }
        

        return redirect()->route('employees.index')
                         ->with([
                                'message' => 'Successfully created Employee.',
                                'alert-type' => 'success'
                            ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        $departments = $this->department->all();
        return view('employees.edit' , ['user' => $user , 'departments' => $departments]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $employee = User::find($id);
        $request->validate([
            'name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users,email,' . $employee->id,
            'phone' => 'required|string|unique:users,phone,' . $employee->id,

        ]);
        
        $employee->name = $request->name;
        $employee->email = $request->email;
        $employee->phone = $request->phone;
        $employee->save();
        return redirect()->route('employees.index')->with(['message' => 'Successfully Updated!' , 'alert-type' => 'success']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $user = User::find($id);
        $user->delete();
        return redirect()->route('employees.index')->with(['message' => 'Successfully Deleted!' , 'alert-type' => 'success']);
    }
}
