@extends('layouts.app')

@section('content')

    <div class="page">
		<div class="page-content container">
			<div class="row">
				<div class="col-md-6">
					<div class="panel panel-primary panel-line">
						<div class="panel-heading">
							<h3 class="panel-title">
								{{ __('Update Department')}}
							</h3>
						</div>
						<div class="panel-body">
							<form autocomplete="off" method="POST" action="{{ route('departments.update' , ['id' => $department->id]) }}">
								{{ csrf_field() }}
                                {{ method_field('PATCH')}}
	                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
	                                <label for="name" class="col-md-4 control-label">{{ __('Name') }}</label>
	                                <div class="col-md-12">
	                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ $department->name }}" name="name"  required autofocus>

	                                    @if ($errors->has('name'))
	                                        <span class="invalid-feedback">
	                                            <strong>{{ $errors->first('name') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
                            	</div>
								<button id="deptSubmit" type="submit" class="btn btn-primary ladda-button waves-effect waves-light waves-round" data-style="slide-right" data-plugin="ladda">
                        			<span class="ladda-label">{{ __('Update') }}<i class="icon md-long-arrow-right ml-10" aria-hidden="true"></i></span>
                      				<span class="ladda-spinner"></span><div class="ladda-progress" style="width: 0px;"></div>
                      			</button>
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
@endsection