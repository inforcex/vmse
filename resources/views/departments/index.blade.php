@extends('layouts.app')

@section('page-level-styles')
	<link rel="stylesheet" href="{{ asset('admin/vendor/datatables.net-bs4/dataTables.bootstrap4.min.css') }}">
@endsection

@section('content')
	<div class="page">
		<div class="page-content container">
			<div class="panel">
          <header class="panel-heading">
            <h3 class="panel-title">{{ __('Departments') }} <a href="{{ route('departments.create') }}" class="btn btn-primary create-btn">{{ __('Add Department') }}</a></h3>
          </header>
          <div class="panel-body">
            <table class="table table-hover dataTable table-striped w-full" id="departmentsTable">
              <thead>
                <tr>
                  <th>{{ __('Name') }}</th>
                  <th>{{ __('Actions') }}</th>
                </tr>
              </thead>
              <tbody>
              </tbody>
            </table>
          </div>
        </div>
        <!-- End Panel Table Tools -->
		</div>
	</div>
@endsection

@section('page-level-scripts')

    

	<script src="{{ asset('admin/vendor/datatables.net/jquery.dataTables.js') }}"></script>
    <script src="{{ asset('admin/vendor/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
    <script src="{{ asset('admin/vendor/datatables.net-buttons/dataTables.buttons.min.js') }}"></script>
    <script src="{{ asset('admin/vendor/datatables.net-buttons/buttons.html5.min.js') }}"></script>
    <script src="{{ asset('admin/vendor/datatables.net-buttons/buttons.print.min.js') }}"></script>
    <script src="{{ asset('admin/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.min.js')}}"></script>
    <script>
    	$(document).ready(function(){
    		$('#departmentsTable').DataTable({
          language : {
						paginate : {
							previous : "{!! __('Previous') !!}",
							next : "{!! __('Next') !!}"
						},
						search : "{!! __('Search') !!}",
					},
    			processing : true,
    			serverSide : true,
    			ajax : '{!! route('datatable.departments') !!}',
    			columns: [
		            { data: 'name', render : function(data, type, full, meta){
                    return '<strong>' + data + '</strong>';
                }},
		            { data : 'action' , name : 'action' , orderable: false, searchable: false}
		        ],
		        {{--  sDom: '<"dt-panelmenu clearfix"Bfr>t<"dt-panelfooter clearfix"ip>',
		        buttons: ['csv', 'print']  --}}
    		});
    	});
    </script>
@endsection