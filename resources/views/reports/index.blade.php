@extends('layouts.app') 
@section('page-level-styles')
    <link rel="stylesheet" href="{{ asset('admin/examples/css/charts/chartjs.css') }}"> 
    <link rel="stylesheet" href="{{ asset('admin/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/vendor/ladda/ladda.min.css') }}">
@endsection
@section('content')
<div class="page">
	<div class="page-content container-fluid">
        <div class="row">
            <div class="col-md-6 col-xl-4">
                    <div class="input-daterange clearfix" data-plugin="datepicker" data-format="yyyy-mm-dd">
                      <div class="input-group">
                        <span class="input-group-addon">
                          <i class="icon md-calendar" aria-hidden="true"></i>
                        </span>
                        <input type="text" class="form-control" name="start"  id="start"/>
                      </div>
                      <div class="input-group">
                        <span class="input-group-addon">to</span>
                        <input type="text" class="form-control" name="end" id="end" />
                      </div>
                      <div class="float-right">
                      <button type="button" class="btn btn-primary ladda-button filter-button">
                        <span class="ladda-label">Apply</span>
                      </button>
                    </div>
                    </div>
            </div>
        </div>
		<canvas id="visitorChart" style="width:100%;height:350px;"></canvas>
	</div>
</div>
@endsection
@section('page-level-scripts')
<script src="{{ asset('admin/vendor/ladda/spin.min.js') }}"></script>
<script src="{{ asset('admin/vendor/ladda/ladda.min.js') }}"></script>
<script src="{{ asset('admin/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
<script src="{{ asset('admin/vendor/chart-js/Chart.min.js') }}"></script>
{{--  <script src="{{ asset('admin/js/Plugin/ladda.js') }}"></script>  --}}
<script>
	$(document).ready(function(){
            var chartData = {
                labels : @json($labels),
                datasets: [{
                    label: "Visitors",
                    fill: true,
                    backgroundColor: "rgba(98, 168, 234, .1)",
                    borderColor: "#B68A35",
                    pointRadius: 4,
                    borderDashOffset: 2,
                    pointBorderColor: "#fff",
                    pointBackgroundColor: "#B68A35",
                    pointHoverBackgroundColor: "#fff",
                    pointHoverBorderColor: "#B68A35",
                    data: @json($visitors)
                }]
            };

            var myLine = new Chart(document.getElementById("visitorChart").getContext("2d"), {
                    type: 'line',
                    data: chartData,
                    options: {
                        responsive: true,
                        scales: {
                        xAxes: [{
                            display: true
                        }],
                        yAxes: [{
                            display: true
                        }]
                        }
                    }
            });
            var l = Ladda.create( document.querySelector( '.filter-button' ) );
            $('body').on('click' , '.filter-button',  function(event){
                $.ajax({
                    url : "{!! route('visitors.by.date') !!}",
                    method : 'GET',
                    data : {start : $('#start').val() , end : $('#end').val()},
                    beforeSend : function(){
                        l.start();
                    }
                }).done(function(response){
                    console.log(response);
                    myLine.data.datasets[0].data = response.values;
                    myLine.data.datasets[0].labels = response.labels;
                    myLine.update();
                }).always(function(){
                    l.stop();
                });
            });
        });
</script>
@endsection