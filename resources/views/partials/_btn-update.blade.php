<button type="submit" class="btn btn-primary ladda-button waves-effect waves-light waves-round" data-style="slide-right" data-plugin="ladda">
	<span class="ladda-label">{{ __('Update') }}<i class="icon md-long-arrow-right ml-10" aria-hidden="true"></i></span>
	<span class="ladda-spinner"></span>
	<div class="ladda-progress" style="width: 0px;"></div>
</button>