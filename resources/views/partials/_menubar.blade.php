<div class="site-menubar">
	<div class="site-menubar-body">
		<div>
			<div>
				<ul class="site-menu" data-plugin="menu">
					<li class="site-menu-item">
						<a href="{{ url('/home') }}" dropdown-badge="false">
							<span class="site-menu-title">{{ __('Dashboard')}} </span>
							<img src="{{ asset('admin/images/home.png')}}" class="site-menu-item-img" alt="">
						</a>
					</li>
					<li class="site-menu-item">
						<a href="{{ url('/visitors') }}" dropdown-badge="false">
							<span class="site-menu-title">{{ __('Visitors')}} </span>
							<img src="{{ asset('admin/images/visitors.png')}}" class="site-menu-item-img" alt="">
						</a>
					</li>
					<li class="site-menu-item">
						<a href="{{ url('/employees') }}" dropdown-badge="false">
							<span class="site-menu-title">{{ __('Employees')}} </span>
							<img src="{{ asset('admin/images/employee.png')}}" class="site-menu-item-img" alt="">
						</a>
					</li>
					<li class="site-menu-item">
						<a href="{{ url('/admins') }}" dropdown-badge="false">
							<span class="site-menu-title">{{ __('Admins')}} </span>
							<img src="{{ asset('admin/images/admin.png')}}" class="site-menu-item-img" alt="">
						</a>
					</li>
					<li class="site-menu-item">
						<a href="{{ url('/departments') }}" dropdown-badge="false">
							<span class="site-menu-title">{{ __('Departments')}} </span>
							<img src="{{ asset('admin/images/sections.png')}}" class="site-menu-item-img" alt="">
						</a>
					</li>
					<li class="site-menu-item">
						<a href="{{ url('/settings') }}" dropdown-badge="false">
							<span class="site-menu-title">{{ __('Settings')}} </span>
							<img src="{{ asset('admin/images/settings.png')}}" class="site-menu-item-img" alt="">
						</a>
					</li>
					<li class="site-menu-item">
						<a href="{{ url('/reports') }}" dropdown-badge="false">
							<span class="site-menu-title">{{ __('Reports')}} </span>
							<img src="{{ asset('admin/images/reports.png')}}" class="site-menu-item-img" alt="">
						</a>
					</li>
					{{--  @if(auth()->user()->hasRole(['super_admin' , 'admin']))
					<li class="site-menu-item has-sub">
						<a href="javascript:void(0)" dropdown-badge="false">
							<i class="site-menu-icon md-assignment-account" aria-hidden="true"></i>
							<span class="site-menu-title">Admins</span>
							<span class="site-menu-arrow"></span>
						</a>
						<ul class="site-menu-sub">
							<li class="site-menu-item">
								<a class="animsition-link" href="{{ route('admins.create')}}">
									<span class="site-menu-title">Add Admin</span>
								</a>
							</li>
							<li class="site-menu-item">
								<a class="animsition-link" href="{{ route('admins.index')}}">
									<span class="site-menu-title">View Admins</span>
								</a>
							</li>
						</ul>
					</li>
					@endif
					@permission('create_employee')
					<li class="site-menu-category">Employees</li>
					<li class="site-menu-item has-sub">
						<a href="javascript:void(0)" dropdown-badge="false">
							<i class="site-menu-icon md-nature-people" aria-hidden="true"></i>
							<span class="site-menu-title">Employees</span>
							<span class="site-menu-arrow"></span>
						</a>
						<ul class="site-menu-sub">
							<li class="site-menu-item">
								<a class="animsition-link" href="{{ route('employees.create')}}">
									<span class="site-menu-title">Add Employees</span>
								</a>
							</li>
							<li class="site-menu-item">
								<a class="animsition-link" href="{{ route('employees.index')}}">
									<span class="site-menu-title">View Employees</span>
								</a>
							</li>
						</ul>
					</li>
					@endpermission
					<li class="site-menu-item has-sub">
						<a href="javascript:void(0)">
							<i class="site-menu-icon md-group-work" aria-hidden="true"></i>
							<span class="site-menu-title">Visitors</span>
							<span class="site-menu-arrow"></span>
						</a>
						<ul class="site-menu-sub">
							<li class="site-menu-item">
								<a class="animsition-link" href="{{ route('visitors.create')}}">
									<span class="site-menu-title">Invite Visitor</span>
								</a>
							</li>
							@if(auth()->user()->hasRole(['employee' , 'admin']))
								<li class="site-menu-item">
									<a class="animsition-link" href="{{ route('my.visitors')}}">
										<span class="site-menu-title">My Visitors</span>
									</a>
								</li>
							@endif
							@if(auth()->user()->hasRole(['super_admin' , 'admin']))
								<li class="site-menu-item">
									<a class="animsition-link" href="{{ route('visitors.index')}}">
										<span class="site-menu-title">View All Visitors</span>
									</a>
								</li>
							@endif
						</ul>
					</li>
					<li class="site-menu-item has-sub">
						<a href="javascript:void(0)" dropdown-badge="false">
							<i class="site-menu-icon md-view-comfy" aria-hidden="true"></i>
							<span class="site-menu-title">Departments</span>
							<span class="site-menu-arrow"></span>
						</a>
						<ul class="site-menu-sub">
							<li class="site-menu-item">
								<a class="animsition-link" href="{{ route('departments.create')}}">
									<span class="site-menu-title">Add Department</span>
								</a>
							</li>
							<li class="site-menu-item">
								<a class="animsition-link" href="{{ route('departments.index')}}">
									<span class="site-menu-title">View Departments</span>
								</a>
							</li>
						</ul>
					</li>  --}}
				</ul>
			</div>
		</div>
	</div>
</div>