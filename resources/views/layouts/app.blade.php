<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=0, minimal-ui">
    <meta name="description" content="bootstrap admin template">
    <meta name="author" content="">
	<title>Venvoy</title>
	@include('layouts.partials._head-styles')
	@yield('page-level-styles')
	<link rel="stylesheet" type="text/css" href="{{ asset('admin/vendor/toastr/toastr.min.css')}}">
	@if(app()->getLocale() == 'en')
		<link rel="stylesheet" href="{{ asset('css/app-ltr.css') }}">
	@else
		<link rel="stylesheet" href="{{ asset('css/app-rtl.css') }}">
	@endif
	<style>
		#phone{
			direction : ltr;
		}
		#dob{
			direction : ltr;
		}
	</style>
</head>
<body class="animsition site-navbar-small dashboard"> 
	@include('partials._nav')
	@include('partials._menubar')
	@yield('content')
	@include('layouts.partials._footer-scripts')
	@yield('page-level-scripts')
	@if(app()->getLocale() == 'ar')
		<style>
			.form-control {
				direction: rtl;
			}
		</style>
	@endif
	<script src="{{ asset('admin/vendor/toastr/toastr.min.js')}}"></script>
	<script type="text/javascript">
		$(document).ready(function(){
			@if(Session::has('message'))
				var alertType = {!! json_encode(Session::get('alert-type', 'info')) !!};
				var alertMessage = {!! json_encode(Session::get('message')) !!};
				var alerter = toastr[alertType];
				if (alerter) {
	        		alerter(alertMessage);
			    } else {
			        toastr.error("toastr alert-type " + alertType + " is unknown");
			    }
			@endif
		});
		
	</script>
</body>
</html>