<!-- Core  -->
<script src="{{ asset('admin/vendor/babel-external-helpers/babel-external-helpers.js') }}"></script>
<script src="{{ asset('admin/vendor/jquery/jquery.js') }}"></script>
<script src="{{ asset('admin/vendor/popper-js/umd/popper.min.js') }}"></script>
<script src="{{ asset('admin/vendor/bootstrap/bootstrap.js') }}"></script>
<script src="{{ asset('admin/vendor/animsition/animsition.js') }}"></script>
<script src="{{ asset('admin/vendor/mousewheel/jquery.mousewheel.js') }}"></script>
<script src="{{ asset('admin/vendor/asscrollbar/jquery-asScrollbar.js') }}"></script>
<script src="{{ asset('admin/vendor/asscrollable/jquery-asScrollable.js') }}"></script>
<script src="{{ asset('admin/vendor/waves/waves.js') }}"></script>

<!-- Plugins -->
<script src="{{ asset('admin/vendor/switchery/switchery.js') }}"></script>
<script src="{{ asset('admin/vendor/intro-js/intro.js') }}"></script>
<script src="{{ asset('admin/vendor/screenfull/screenfull.js') }}"></script>
<script src="{{ asset('admin/vendor/slidepanel/jquery-slidePanel.js') }}"></script>
{{--
<script src="{{ asset('admin/vendor/chartist/chartist.min.js') }}"></script> --}}
<script src="{{ asset('admin/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.js') }}"></script>
<script src="{{ asset('admin/vendor/jvectormap/jquery-jvectormap.min.js') }}"></script>
<script src="{{ asset('admin/vendor/jvectormap/maps/jquery-jvectormap-world-mill-en.js') }}"></script>
<script src="{{ asset('admin/vendor/matchheight/jquery.matchHeight-min.js') }}"></script>
<script src="{{ asset('admin/vendor/peity/jquery.peity.min.js') }}"></script>

<!-- Scripts -->
<script src="{{ asset('admin/js/Component.js') }}"></script>
<script src="{{ asset('admin/js/Plugin.js') }}"></script>
<script src="{{ asset('admin/js/Base.js') }}"></script>
<script src="{{ asset('admin/js/Config.js') }}"></script>

<script src="{{ asset('admin/js/Section/Menubar.js') }}"></script>
<script src="{{ asset('admin/js/Section/Sidebar.js') }}"></script>
<script src="{{ asset('admin/js/Section/PageAside.js') }}"></script>
<script src="{{ asset('admin/js/Plugin/menu.js') }}"></script>

<!-- Config -->
<script src="{{ asset('admin/js/config/colors.js') }}"></script>
<script src="{{ asset('admin/js/config/tour.js') }}"></script>
<script>
	Config.set('assets', '../admin');

</script>

<!-- Page -->
<script src="{{ asset('admin/js/Site.js') }}"></script>
<script src="{{ asset('admin/js/Plugin/asscrollable.js') }}"></script>
<script src="{{ asset('admin/js/Plugin/slidepanel.js') }}"></script>
<script src="{{ asset('admin/js/Plugin/switchery.js') }}"></script>
<script src="{{ asset('admin/js/Plugin/jquery-placeholder.js') }}"></script>
<script src="{{ asset('admin/js/Plugin/material.js') }}"></script>
<script src="{{ asset('admin/js/Plugin/matchheight.js') }}"></script>
<script src="{{ asset('admin/js/Plugin/jvectormap.js') }}"></script>
<script src="{{ asset('admin/js/Plugin/peity.js') }}"></script>

<script src="{{ asset('admin/examples/js/dashboard/v1.js') }}"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<script src="{{ asset('js/app.js') }}"></script>
