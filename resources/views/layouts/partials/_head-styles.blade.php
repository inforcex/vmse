@if(app()->getLocale() == 'en')
    <!-- Stylesheets -->
    <link rel="stylesheet" href="{{ asset('admin/css/bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/bootstrap-extend.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/site.min.css') }}">
@else
    <link rel="stylesheet" href="{{ asset('admin/css/rtl/bootstrap-rtl.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/rtl/bootstrap-extend-rtl.min.css') }}">
    <link rel="stylesheet" href="{{ asset('admin/css/rtl/site-rtl.min.css') }}">
@endif
<!-- Plugins -->
<link rel="stylesheet" href="{{ asset('admin/vendor/animsition/animsition.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendor/asscrollable/asScrollable.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendor/switchery/switchery.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendor/intro-js/introjs.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendor/slidepanel/slidePanel.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendor/flag-icon-css/flag-icon.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendor/waves/waves.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendor/chartist/chartist.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendor/jvectormap/jquery-jvectormap.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.css') }}">
<link rel="stylesheet" href="{{ asset('admin/examples/css/dashboard/v1.css') }}">


<!-- Fonts -->
<link rel="stylesheet" href="{{ asset('admin/fonts/material-design/material-design.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/fonts/web-icons/web-icons.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/fonts/brand-icons/brand-icons.min.css') }}">
<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

<!--[if lt IE 9]>
    <script src="{{ asset('admin/vendor/html5shiv/html5shiv.min.js') }}"></script>
    <![endif]-->

<!--[if lt IE 10]>
    <script src="{{ asset('admin/vendor/media-match/media.match.min.js') }}"></script>
    <script src="{{ asset('admin/vendor/respond/respond.min.js') }}"></script>
    <![endif]-->

<!-- Scripts -->
<script src="{{ asset('admin/vendor/breakpoints/breakpoints.js') }}"></script>
<script>
	Breakpoints();
</script>
<link rel="stylesheet" href="{{ asset('css/app.css') }}">
