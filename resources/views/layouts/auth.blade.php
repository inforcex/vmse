<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>Venvoy</title>

    @include('layouts.partials._head-styles')
    @yield('page-level-styles')
</head>
        @yield('content')
    <!-- Scripts -->
    @include('layouts.partials._footer-scripts')
    {{-- <script src="{{ asset('js/app.js') }}"></script> --}}
    @yield('page-level-scripts')
</body>
</html>
