@extends('layouts.auth') @section('page-level-styles')
<link rel="stylesheet" href="{{ asset('css/intlTelInput.css')}}">
<link rel="stylesheet" href="{{ asset('admin/examples/css/pages/register-v3.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendor/formvalidation/formValidation.css') }}">
<link rel="stylesheet" href="{{ asset('admin/examples/css/forms/validation.css') }}"> @endsection @section('content')
<!-- Page -->

<body class="animsition page-register-v3 layout-full">
	<div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">>
		<div class="page-content vertical-align-middle">
			<div class="panel">
				<div class="panel-body">
					<div class="brand">
						<img class="brand-img" src="{{ asset('admin/images/logo-blue.png') }}" alt="...">
						<h2 class="brand-text font-size-18">Venvoy</h2>
					</div>
					<form method="post" action="{{ route('register') }}" autocomplete="off">
						{{ csrf_field() }}
						<div class="form-group form-material floating{{ $errors->has('name') ? ' has-danger' : '' }}" data-plugin="formMaterial">
							<input type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name" value="{{ old('name') }}"
							 required/>
							<label class="floating-label">Full Name</label>
							@if ($errors->has('name'))
							<div class="invalid-feedback">
								<strong>{{ $errors->first('name') }}</strong>
							</div>
							@endif
						</div>
						<div class="form-group form-material floating{{ $errors->has('email') ? ' has-danger' : '' }}" data-plugin="formMaterial">
							<input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"
							 required/>
							<label class="floating-label">Email</label>
							@if ($errors->has('email'))
							<div class="invalid-feedback">
								<strong>{{ $errors->first('email') }}</strong>
							</div>
							@endif
						</div>
						<div class="form-group form-material floating{{ $errors->has('phone') ? ' has-danger' : '' }}" data-plugin="formMaterial">
							<input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" value="{{ old('phone') }}"
							 required/> {{--
							<label class="floating-label">Phone</label> --}} @if ($errors->has('phone'))
							<div class="invalid-feedback">
								<strong>{{ $errors->first('phone') }}</strong>
							</div>
							@endif
						</div>
						<div class="form-group form-material floating{{ $errors->has('password') ? ' has-danger' : '' }}" data-plugin="formMaterial">
							<input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required/>
							<label class="floating-label">Password</label>
							@if ($errors->has('password'))
							<div class="invalid-feedback">
								<strong>{{ $errors->first('password') }}</strong>
							</div>
							@endif
						</div>
						<button type="submit" class="btn btn-primary btn-block btn-lg mt-40">Sign up</button>
					</form>
					<p>Have account already? Please go to
						<a href="{{ route('login') }}">Sign In</a>
					</p>
				</div>
			</div>

			<footer class="page-copyright page-copyright-inverse">
				<p>Venoy</p>
				<p>© 2018. All RIGHT RESERVED.</p>
				<div class="social">
					<a class="btn btn-icon btn-pure" href="javascript:void(0)">
						<i class="icon bd-twitter" aria-hidden="true"></i>
					</a>
					<a class="btn btn-icon btn-pure" href="javascript:void(0)">
						<i class="icon bd-facebook" aria-hidden="true"></i>
					</a>
					<a class="btn btn-icon btn-pure" href="javascript:void(0)">
						<i class="icon bd-google-plus" aria-hidden="true"></i>
					</a>
				</div>
			</footer>
		</div>
	</div>
	<!-- End Page -->
	@endsection @section('page-level-scripts')
	<script src="{{asset('js/intlTelInput.min.js')}}"></script>
	<script>
		$(document).ready(function(){
              var telInput = $("#phone"),
              errorMsg = $("#error-msg"),
              validMsg = $("#valid-msg");

            // initialise plugin
            telInput.intlTelInput({
              utilsScript: "{{ asset('js/utils.js')}}"
            });

            var reset = function() {
              telInput.removeClass("error");
              errorMsg.addClass("hide");
              validMsg.addClass("hide");
            };

            // on blur: validate
            telInput.blur(function() {
              reset();
              if ($.trim(telInput.val())) {
                if (telInput.intlTelInput("isValidNumber")) {
                  validMsg.removeClass("hide");
                  $('#registerSubmit').removeAttr('disabled');
                } else {
                  telInput.addClass("error");
                  errorMsg.removeClass("hide");
                }
              }
            });

            // on keyup / change flag: reset
            telInput.on("keyup change", reset);
        });

	</script>
	@endsection