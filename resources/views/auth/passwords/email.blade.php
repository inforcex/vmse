@extends('layouts.auth') @section('page-leve-styles')
<link rel="stylesheet" href="{{ asset('admin/examples/css/pages/forgot-password.css') }}"> @endsection @section('content')

<body class="animsition page-forgot-password layout-full">
	<!--[if lt IE 8]>
        <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
    <![endif]-->


	<!-- Page -->
	<div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
		<div class="page-content vertical-align-middle">
			<h2>Forgot Your Password ?</h2>
			<p>Input your registered email to reset your password</p>
			@if (session('status'))
                <div class="alert alert-success">
                    {{ session('status') }}
                </div>
			@endif
			<form method="post" role="form" autocomplete="off" action="{{ route('password.email') }}">
				{{ csrf_field() }}
				<div class="form-group form-material floating{{ $errors->has('email') ? ' has-danger' : '' }}" data-plugin="formMaterial">
					<input type="email" class="form-control empty{{ $errors->has('email') ? ' is-invalid' : '' }}" id="inputEmail" name="email">
					<label class="floating-label" for="inputEmail">Your Email</label>
					@if ($errors->has('email'))
					<span class="invalid-feedback">
						<strong>{{ $errors->first('email') }}</strong>
					</span>
					@endif
				</div>
				<div class="form-group">
					<button type="submit" class="btn btn-primary btn-block">Reset Your Password</button>
				</div>
			</form>

			<footer class="page-copyright">
				<p>Venvoy</p>
				<p>© 2018. All RIGHT RESERVED.</p>
				<div class="social">
					<a class="btn btn-icon btn-pure" href="javascript:void(0)">
						<i class="icon bd-twitter" aria-hidden="true"></i>
					</a>
					<a class="btn btn-icon btn-pure" href="javascript:void(0)">
						<i class="icon bd-facebook" aria-hidden="true"></i>
					</a>
					<a class="btn btn-icon btn-pure" href="javascript:void(0)">
						<i class="icon bd-google-plus" aria-hidden="true"></i>
					</a>
				</div>
			</footer>
		</div>
	</div>
	<!-- End Page -->
	@endsection