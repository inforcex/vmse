@extends('layouts.auth') @section('page-level-styles')

<link rel="stylesheet" href="{{ asset('admin/examples/css/pages/login-v3.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendor/formvalidation/formValidation.css') }}">
<link rel="stylesheet" href="{{ asset('admin/examples/css/forms/validation.css') }}">
<style>
	.btn-primary{
		background-color: #B68A35;
    	border-color: #B68A35
	}
	.btn-primary:hover{
		background-color: #B68A35;
    	border-color: #B68A35;
	}
</style> 
@endsection 

@section('content')
<!-- Page -->
<body class="animsition page-login-v3 layout-full">
<div class="page vertical-align text-center" data-animsition-in="fade-in" data-animsition-out="fade-out">
	<div class="page-content vertical-align-middle">
		<div class="panel">
			<div class="panel-body">
				<div class="brand">
					<img class="brand-img" src="{{ asset('admin/images/logo-blue.png') }}" alt="..."> 
					<h2 class="brand-text font-size-18">Venvoy</h2>
				</div>
				<form method="post" action="{{ route('login') }}" autocomplete="off">
					{{ csrf_field() }}
					<div class="form-group form-material floating{{ $errors->has('email') ? ' has-danger' : '' }}" data-plugin="formMaterial">
						<input type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email" value="{{ old('email') }}"
						/>
						<label class="floating-label">Email</label>
						@if ($errors->has('email'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('email') }}</strong>
                            </div>
						@endif
					</div>
					<div class="form-group form-material floating{{ $errors->has('password') ? ' has-danger' : '' }}" data-plugin="formMaterial">
						<input type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" />
						<label class="floating-label">Password</label>
                        @if ($errors->has('password'))
                            <div class="invalid-feedback">
                                <strong>{{ $errors->first('password') }}</strong>
                            </div>
						@endif
					</div>
					<div class="form-group clearfix">
						<div class="checkbox-custom checkbox-inline checkbox-primary checkbox-lg float-left">
							<input type="checkbox" id="inputCheckbox" name="remember">
							<label for="inputCheckbox">Remember me</label>
						</div>
						{{--  <a class="float-right" href="{{ route('password.request') }}">Forgot password?</a>  --}}
					</div>
					<button type="submit" class="btn btn-primary btn-block btn-lg mt-40">Sign in</button>
				</form>
				{{--  <p>Still no account? Please go to
					<a href="{{ route('register') }}">Sign up</a>
				</p>  --}}
			</div>
		</div>

		<footer class="page-copyright page-copyright-inverse">
			<p>Venvoy</p>
			<p>© 2018. All RIGHT RESERVED.</p>
			{{--  <div class="social">
				<a class="btn btn-icon btn-pure" href="javascript:void(0)">
					<i class="icon bd-twitter" aria-hidden="true"></i>
				</a>
				<a class="btn btn-icon btn-pure" href="javascript:void(0)">
					<i class="icon bd-facebook" aria-hidden="true"></i>
				</a>
				<a class="btn btn-icon btn-pure" href="javascript:void(0)">
					<i class="icon bd-google-plus" aria-hidden="true"></i>
				</a>
			</div>  --}}
		</footer>
	</div>
</div>
<!-- End Page -->
@endsection @section('page-level-scripts')
<script src="{{ asset('admin/vendor/formvalidation/formValidation.min.js') }}"></script>
<script src="{{ asset('admin/vendor/formvalidation/framework/bootstrap4.min.js') }}"></script>
<script src="{{ asset('admin/examples/js/forms/validation.js') }}"></script>
@endsection