<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1">

	<!-- CSRF Token -->
	<meta name="csrf-token" content="{{ csrf_token() }}">
	<title>Company's Visitor Check In</title>
	<!-- Stylesheets -->
	<link rel="stylesheet" href="{{ asset('admin/css/bootstrap.min.css') }}">
	<link rel="stylesheet" href="{{ asset('admin/css/bootstrap-extend.min.css') }}">
	<link rel="stylesheet" href="{{ asset('admin/css/site.min.css') }}">
	<!-- Plugins -->
	<link rel="stylesheet" href="{{ asset('admin/vendor/animsition/animsition.min.css') }}">
	<link rel="stylesheet" href="{{ asset('admin/vendor/asscrollable/asScrollable.min.css') }}">
	<link rel="stylesheet" href="{{ asset('admin/vendor/switchery/switchery.min.css') }}">
	<link rel="stylesheet" href="{{ asset('admin/vendor/intro-js/introjs.min.css') }}">
	<link rel="stylesheet" href="{{ asset('admin/vendor/slidepanel/slidePanel.min.css') }}">
	<link rel="stylesheet" href="{{ asset('admin/vendor/flag-icon-css/flag-icon.min.css')}}">
	<link rel="stylesheet" href="{{ asset('admin/vendor/waves/waves.min.css') }}">
	<link rel="stylesheet" href="{{ asset('admin/vendor/toastr/toastr.min.css') }}">
	<link rel="stylesheet" href="{{ asset('admin/vendor/jquery-wizard/jquery-wizard.min.css') }}">
	<link rel="stylesheet" href="{{ asset('admin/vendor/formvalidation/formValidation.min.css') }}">
	<link rel="stylesheet" href="{{ asset('admin/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
	<link rel="stylesheet" href="{{ asset('admin/vendor/timepicker/jquery-timepicker.min.css') }}">
	<link rel="stylesheet" href="{{ asset('admin/vendor/bootstrap-select/bootstrap-select.min.css') }}">
	<!-- Fonts -->
	<link rel="stylesheet" href="{{ asset('admin/fonts/material-design/material-design.min.css') }}">
	<link rel="stylesheet" href="{{ asset('admin/fonts/brand-icons/brand-icons.min.css') }}">
	<link rel='stylesheet' href='http://fonts.googleapis.com/css?family=Roboto:300,400,500,300italic'>

	<!--[if lt IE 9]>
	<script src="{{ asset('admin/vendor/html5shiv/html5shiv.min.js') }}"></script>
	<![endif]-->

	<!--[if lt IE 10]>
	<script src="{{ asset('admin/vendor/media-match/media.match.min.js') }}"></script>
	<script src="{{ asset('admin/vendor/respond/respond.min.js') }}"></script>
	<![endif]-->

	<!-- Scripts -->
	<script src="{{ asset('admin/vendor/breakpoints/breakpoints.min.js') }}"></script>
	<script>
		Breakpoints();
	</script>
	<link rel="stylesheet" href="{{ asset('css/app.css') }}">
    <link rel="stylesheet" href="{{ asset('css/app-rtl.css') }}">
	<link rel="stylesheet" href="{{ asset('css/intlTelInput.css')}}">
	<link rel="stylesheet" href="{{ asset('admin/vendor/select2/select2.min.css') }}">
    <style>
        body{
            padding-top : 0;
        }
        .form-control{
            direction : rtl;
        }
        label{
            float : right;
        }
        #visitorPhone{
			direction : ltr;
		}
    </style>
</head>

<body>
	<div class="loading">
	</div>
	<div class="page">
		<div class="page-content container">
			<div class="row">
				<div class="col-lg-12 text-center">
					<div class="card card-shadow text-center">
						<div class="card-block">
							@if($company->avatar_id)
							<a href="javascript:void(0)" class="avatar avatar-lg" style="width: 130px;">
								<img src="{{ asset('images/avatars' . $company->avatar->path) }}" alt="Company Avatar">
							</a>
							@else
							<h4 class="profile-user">
								{{ $company->name }}
							</h4>
							@endif
							<p class="profile-job">
								{{ $company->welcome_message}}
							</p>
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-12">

					<div class="panel" id="exampleWizardFormContainer">
						<div class="panel-heading clearfix">
							<h3 class="panel-title float-right">مرحبا، الزائر
                                &nbsp;<a href="{{ route('visitors.front' , ['identifier' => $company->identifier])}}?lang=en">
									<span class="flag-icon flag-icon-gb"></span>
								</a>
                            </h3>
                            
						</div>
						<div class="panel-body">
							<!-- Steps -->
							<div class="pearls row">
								<div class="pearl current col-4">
									<div class="pearl-icon">
										<i class="icon md-account" aria-hidden="true"></i>
									</div>
									<span class="pearl-title">معلومات شخصية</span>
								</div>
								<div class="pearl col-4">
									<div class="pearl-icon">
										<i class="icon md-account" aria-hidden="true"></i>
									</div>
									<span class="pearl-title">زيارة معلومات</span>
								</div>
								<div class="pearl col-4">
									<div class="pearl-icon">
										<i class="icon md-check" aria-hidden="true"></i>
									</div>
									<span class="pearl-title">التأكيد</span>
								</div>
							</div>
							<!-- End Steps -->
							<!-- Wizard Content -->
							<form id="exampleFormContainer">
								<div class="wizard-content">
									<div class="wizard-pane active" role="tabpanel">
										<input type="hidden" name="tenant_id" value="{{ $company->id }}">
										<input type="hidden" name="front" value="1">
										<div class="row">
											<div class="col-md-4">
												<div class="form-group form-material">
													<label class="form-control-label" for="visitorFirstName">الاسم الاول</label>
													<input type="text" class="form-control" id="visitorFirstName" name="first_name" required="required">
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group form-material">
													<label class="form-control-label" for="visitorMiddleName">الاسم الثاني</label>
													<input type="text" class="form-control" id="visitorMiddleName" name="middle_name">
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group form-material">
													<label class="form-control-label" for="visitorLastName">اسم العائلة</label>
													<input type="text" class="form-control" id="visitorLastName" name="last_name" required="required">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4">
												<div class="form-group form-material">
													<label class="form-control-label" for="visitorPhone">هاتف</label>
													<input type="text" class="form-control" id="visitorPhone" name="phone" required="required">
													<span id="valid-msg" class="hide">✓ صالح</span>
													<span id="error-msg" class="hide">غير صالحة</span>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group form-material">
													<label class="form-control-label" for="visitorEmail">البريد الإلكتروني</label>
													<input type="email" class="form-control" id="visitorEmail" name="email" required="required">
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group form-material">
													<label class="form-control-label" for="visitorEmail">رقم الهوية الاماراتية</label>
													<input type="text" class="form-control" id="visitiorNationaId" name="national_id" required="required">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4">
												<div class="form-group form-material">
													<label class="form-control-label" for="visitorGender">جنس</label>
													<select name="gender" id="visitorGender" class="form-control" required>
														<option value="">حدد نوع الجنس</option>
														<option value="M">رجل</option>
														<option value="F">النساء</option>
													</select>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group form-material">
													<label class="form-control-label" for="visitorCompany">تاريخ الميلاد</label>
													<input type="text" class="form-control" id="dob" name="dob">
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group form-material">
													<label class="form-control-label" for="visitorNationality">جنسية</label>
													<input type="text" class="form-control" id="visitorNationality" name="nationality">
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4">
												<div class="form-group form-material">
													<label class="form-control-label" for="question1">رقم جواز السفر</label>
													<input type="text" class="form-control" id="passport" name="passport_no">
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group form-material">
													<label class="form-control-label" for="vehicleEmirate">الامارة مركبة</label>
													<input type="text" class="form-control" id="vehicleEmirate" name="emirate_vehicle">
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group form-material">
													<label class="form-control-label" for="plateNumber">رقم المركبة</label>
													<input type="text" class="form-control" id="plateNumber" name="plate_number">
												</div>
											</div>
										</div>
									</div>
									<div class="wizard-pane" id="exampleBillingOne" role="tabpanel">
										<div class="row">
											<div class="col-md-4">
												<div class="form-group form-material">
													<label class="form-control-label datepicker" for="visitDate">تاريخ الزيارة</label>
													<input type="text" class="form-control datepicker" id="visitDate" name="visit_date">
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group form-material">
													<div class="input-group">
														<div class="form-control-wrap">
															<label class="form-control-label" for="visitTime">وقت الزيارة</label>
															<input type="text" class="form-control empty timepicker" id="arrivalTimePicker" name="visit_time">
														</div>
														<span class="input-group-btn" style="align-items:flex-end;">
															<button class="btn btn-primary waves-effect waves-classic" type="button" id="currentTimeBtn">الوقت الحالي</button>
														</span>
													</div>
												</div>
											</div>
											{{--  <div class="col-md-4">
												<div class="form-group form-material">
													<div class="input-group">
														<div class="form-control-wrap">
															<label class="form-control-label" for="visitTime">وقت المغادرة</label>
															<input type="text" class="form-control empty timepicker" id="departureTimePicker" name="departure">
														</div>
													</div>
												</div>
											</div>  --}}
										</div>
										<div class="row">
											<div class="col-md-4">
												<div class="form-group form-material">
													<label class="form-control-label" for="visitorType">نوع الزيارة</label>
													<select name="visitor_type" id="visitorType" class="form-control" required>
														<option value="">Select Visit Type</option>
														@foreach($visitorTypes as $visitType)
															<option value="{{ $visitType->id }}">{{ $visitType->name}} </option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group form-material">
													<label class="form-control-label" for="visitorType">قسم</label>
													<select name="department_id" id="department" class="form-control">
														<option value="">حدد القسم</option>
														@foreach($departments as $department)
														<option value="{{ $department->id }}">{{ $department->name }}</option>
														@endforeach
													</select>
												</div>
											</div>
											<div class="col-md-4">
												<div class="form-group form-material">
													<label class="form-control-label" for="hostSelect">المضيف</label>
													<select class="form-control" name="user_id" id="hostSelect" style="width: 100%" required></select>
												</div>
											</div>
										</div>
										<div class="row">
											<div class="col-md-4">
												<div class="form-group form-material">
													<label class="form-control-label" for="reason">سبب الزيارة</label>
												<textarea name="reason" id="reason" cols="30" rows="7" class="form-control" required></textarea>
												</div>
											</div>
										</div>
									</div>
									<div class="wizard-pane" id="exampleGettingOne" role="tabpanel">
										<div class="text-center my-20">
											<h4>التأكيد</h4>
										</div>
										<div class="text-center my-20">
											<h4 id="visitorInfo"></h4>
											<span> يزور </span>
											<h4 id="hostInfo"></h4>
										</div>
									</div>
								</div>
							</form>
							<!-- Wizard Content -->
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


	<!-- Core Scripts -->
	<script src="{{ asset('admin/vendor/babel-external-helpers/babel-external-helpers.js') }}"></script>
	<script src="{{ asset('admin/vendor/jquery/jquery.min.js') }}"></script>
	<script src="{{ asset('admin/vendor/popper-js/umd/popper.min.js') }}"></script>
	<script src="{{ asset('admin/vendor/bootstrap/bootstrap.min.js') }}"></script>
	<script src="{{ asset('admin/vendor/animsition/animsition.min.js') }}"></script>
	<script src="{{ asset('admin/vendor/mousewheel/jquery.mousewheel.js') }}"></script>
	<script src="{{ asset('admin/vendor/asscrollbar/jquery-asScrollbar.min.js') }}"></script>
	<script src="{{ asset('admin/vendor/asscrollable/jquery-asScrollable.min.js') }}"></script>
	<script src="{{ asset('admin/vendor/ashoverscroll/jquery-asHoverScroll.min.js') }}"></script>
	<script src="{{ asset('admin/vendor/waves/waves.min.js') }} "></script>

	<!-- Plugins Scripts -->
	<script src="{{ asset('admin/vendor/switchery/switchery.min.js')}}"></script>
	<script src="{{ asset('admin/vendor/intro-js/intro.min.js')}}"></script>
	<script src="{{ asset('admin/vendor/screenfull/screenfull.min.js')}}"></script>
	<script src="{{ asset('admin/vendor/slidepanel/jquery-slidePanel.min.js')}}"></script>
	<script src="{{ asset('admin/vendor/formvalidation/formValidation.min.js') }}"></script>
	<script src="{{ asset('admin/vendor/formvalidation/framework/bootstrap.min.js') }}"></script>
	<script src="{{ asset('admin/vendor/matchheight/jquery.matchHeight-min.js')}}"></script>
	<script src="{{ asset('admin/vendor/jquery-wizard/jquery-wizard.min.js') }}"></script>
	<script src="{{ asset('admin/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
	<script src="{{ asset('admin/vendor/timepicker/jquery.timepicker.min.js') }}"></script>
	<script src="{{ asset('admin/vendor/bootstrap-select/bootstrap-select.min.js') }}"></script>

	<script src="{{ asset('admin/js/Component.js') }}"></script>
	<script src="{{ asset('admin/js/Plugin.js') }}"></script>
	<script src="{{ asset('admin/js/Base.js') }}"></script>
	<script src="{{ asset('admin/js/Config.js') }}"></script>

	<script src="{{ asset('admin/js/Section/Menubar.js') }}"></script>
	<script src="{{ asset('admin/js/Section/Sidebar.js') }}"></script>
	<script src="{{ asset('admin/js/Section/PageAside.js') }}"></script>
	<script src="{{ asset('admin/js/Plugin/menu.js') }}"></script>

	<!-- Page -->
	<script src="{{ asset('admin/js/Site.js') }}"></script>
	<script src="{{ asset('admin/js/Plugin/asscrollable.js')}}"></script>
	<script src="{{ asset('admin/js/Plugin/slidepanel.js')}}"></script>
	<script src="{{ asset('admin/js/Plugin/switchery.js')}}"></script>
	<script src="{{ asset('admin/js/Plugin/jquery-wizard.js')}}"></script>
	<script src="{{ asset('admin/js/Plugin/matchheight.js')}}"></script>

	<script src="{{ asset('js/wizard.js') }}"></script>
	<script src="{{asset('js/intlTelInput.min.js')}}"></script>
	<script src="{{ asset('admin/vendor/select2/select2.full.min.js') }}"></script>
	<script src="{{ asset('admin/js/Plugin/select2.js') }}"></script>
	<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
	<script>
		$(document).ready(function(){
			$.ajaxSetup({
			    headers: {
			        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
			    }
			});

			$('.datepicker').datepicker({
				format : 'yyyy-mm-dd',
				startDate : new Date(),
				todayBtn: "linked",
    			todayHighlight: true
			});
			$('#dob').datepicker({
				format : 'yyyy-mm-dd',
			});
			$('.timepicker').timepicker({
				'timeFormat': 'H:i:s'
			});
			$('body').on('click' , '#currentTimeBtn' , function(){
				$('#arrivalTimePicker').timepicker('setTime' , new Date());
			});
			  var tenant = $("#tenant").val();
			  var telInput = $("#visitorPhone"),
			  errorMsg = $("#error-msg"),
			  validMsg = $("#valid-msg");

			// initialise plugin
			telInput.intlTelInput({
			  utilsScript: "{{ asset('js/utils.js')}}"
			});

			var reset = function() {
			  telInput.removeClass("error");
			  errorMsg.addClass("hide");
			  validMsg.addClass("hide");
			};

			// on blur: validate
			telInput.blur(function() {
			  reset();
			  if ($.trim(telInput.val())) {
			    if (telInput.intlTelInput("isValidNumber")) {
			      validMsg.removeClass("hide");
			      $('#visitorSubmit').removeAttr('disabled');
			    } else {
			      telInput.addClass("error");
			      errorMsg.removeClass("hide");
			    }
			  }
			});

			// on keyup / change flag: reset
			telInput.on("keyup change", reset);

			$('#hostSelect').select2({
				ajax : {
					url : function(){
						var department = $('#department :selected').val();
						return '{!! route("employees.search") !!}?tenant={!! $company->id !!}&department=' + department
					},
					method : 'GET',
					dataType : 'json',
					delay: 250,
			    data: function (params) {
			      return {
			        q: params.term
			      };
			    },
			    processResults: function (data, params) {
			      // parse the results into the format expected by Select2
			      // since we are using custom formatting functions we do not need to
			      // alter the remote JSON data, except to indicate that infinite
			      // scrolling can be used
			      var data1 = $.map(data , function(obj){

                            obj.text = obj.text || obj.name
                            return obj;

                        });
			      return {
                            results : data1
                        };
			    },
			    cache: true
			  },
			  placeholder: 'Search Employees',
			  minimumInputLength: 3
			});

			$('body').on('click' , '[data-wizard="finish"]' , function(event){
				//var visitorFormData = new FormData($('#exampleFormContainer'));
				$.ajax({
					url : '{!! route('visitors.store') !!}',
					method : 'POST',
					data : $('#exampleFormContainer').serialize(),
					processData : false,
					beforeSend : function(){
						$('.loading').css('display' , 'block');
					}
				}).done(function(response){
					if(response)
					{
						if(response.type == 'success')
						{
							swal({
							  title: "شكرا لكم",
							  text: "تم إعلام المضيف",
							  icon: "success",
							  button: {
							  	OK : true,
							  },
							}).then((value) => {
								location.reload();
							});
						}else{
							swal({
							  title: "Whoops!",
							  text: "هناك خطأ ما!",
							  icon: "error",
							  button: "OK",
							});
						}
					}
				}).fail(function(){
							swal({
							  title: "Whoops!",
							  text: "هناك خطأ ما!",
							  icon: "error",
							  button: "OK",
							});
				}).always(function(){
					$('.loading').css('display' , 'none');
				});

			});
			$('body').on('click' , '[data-wizard="next"]' , function(event){
				var visitorName = $('#visitorFirstName').val();
				var hostName = $('#hostSelect :selected').text();
				$('#visitorInfo').text(visitorName);
				$('#hostInfo').text(hostName);
			});

		});
	</script>
</body>

</html>