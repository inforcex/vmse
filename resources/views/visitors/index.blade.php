@extends('layouts.app') @section('page-level-styles')
<link rel="stylesheet" href="{{ asset('admin/vendor/datatables.net-bs4/dataTables.bootstrap4.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendor/icheck/icheck.min.css') }}">
<link rel="stylesheet" href="{{ asset('admin/vendor/ladda/ladda.min.css') }}">
@endsection
@section('content')
<div class="page">
	<div class="page-content container-fluid">
		<div class="panel panel-primary panel-line">
			<div class="panel-heading">
				<h3 class="panel-title">{{ __('Visitors') }}
					<a href="{{ route('visitors.create') }}" class="btn btn-primary create-btn">{{ __('Invite Visitor') }}</a>
                    <a class="btn create-btn" href="http://emiratesidv.azurewebsites.net/index.aspx">Emirates ID</a>
				</h3>
			</div>
			<div class="panel-body">
				<table class="table table-hover dataTable table-striped w-full" id="visitorsTable">
					<thead>
						<tr>
							<th>{{ __('Name') }}</th>
							<th>{{ __('Email') }}</th>
							<th>{{ __('Status') }}</th>
							<th>{{ __('Visit Date') }}</th>
							<th>{{ __('Visit Time')}}</th>
							<th>{{ __('Leave Time')}}</th>
							<th>{{ __('Actions') }}</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
		<!-- End Panel Table Tools -->
	</div>
</div>

<div class="modal fade" id="visitorStatusModal" aria-labelledby="exampleFormModalLabel" role="dialog" tabindex="-1" aria-hidden="true"
 style="display: none;">
	<div class="modal-dialog modal-simple">
		<form class="modal-content" id="visitorStatusUpdate">
			<input type="hidden" id="visitorId" value="" name="visitorId">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="exampleFormModalLabel">{{ __('Change Status') }}</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xl-12">
						<ul class="list-group list-group-dividered list-group-full">
							<li class="list-group-item">
								<input type="radio" name="status" value="processing"> {{ __('Processing')}}
							</li>
							<li class="list-group-item">
								<input type="radio" name="status" value="accepted"> {{ __('Accepted')}}
							</li>
							<li class="list-group-item">
								<input type="radio" name="status" value="rejected"> {{ __('Rejected') }}
							</li>
						</ul>
					</div>
					<div class="col-md-12 float-right">
						<button class="btn btn-primary waves-effect waves-classic" type="button" id="visitorStatusBtn">{{ __('Update') }}</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection @section('page-level-scripts')



<script src="{{ asset('admin/vendor/datatables.net/jquery.dataTables.js') }}"></script>
<script src="{{ asset('admin/vendor/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('admin/vendor/datatables.net-buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin/vendor/datatables.net-buttons/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin/vendor/datatables.net-buttons/buttons.print.min.js') }}"></script>
<script src="{{ asset('admin/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('admin/vendor/icheck/icheck.min.js')}}"></script>
<script src="{{ asset('admin/vendor/ladda/ladda.min.js') }}"></script>
<script>
	$(document).ready(function(){
        $('#visitorStatusModal input').iCheck({
          radioClass: 'iradio_flat'
        });
    		var table = $('#visitorsTable').DataTable({
					language : {
						paginate : {
							previous : "{!! __('Previous') !!}",
							next : "{!! __('Next') !!}"
						},
						search : "{!! __('Search') !!}",
					},
    			processing : true,
    			serverSide : true,
    			ajax : '{!! route('datatable.visitors') !!}',
    			columns: [
					{ data: 'first_name', render : function(data, type, full, meta){
						return '<a href="visitors/' + full.id + '"><strong>' + data + '</strong></a>';
					}},
					{ data: 'email', name: 'email' },
					{ data: 'status', name: 'status' },
					{ data: 'visit_date', name: 'visit_date' },
					{ data: 'visit_time', name: 'visit_time' },
					{ data: 'departure', name: 'departure' },
					{ data : 'action' , name : 'action' , orderable: false, searchable: false}
		        ],
		        {{--  sDom: '<"dt-panelmenu clearfix"Bfr>t<"dt-panelfooter clearfix"ip>',
		        buttons: ['csv', 'print']  --}}
    		});
        $('body').on('click' , '.change-status-btn' , function(){
            var visitorId = $(this).attr('data-visitor');
            $('#visitorId').val(visitorId);
						var visitorStatus = $(this).attr('data-status');
						$('#visitorStatusUpdate').find('input[value="' + visitorStatus +'"]').parent('.iradio_flat').addClass('checked');
						
        });
        $('body').on('click' , '#visitorStatusBtn' , function(event){
            $.ajax({
              url : '{!! route('visitor.status.update') !!}',
              data : $('#visitorStatusUpdate').serialize(),
              method : 'GET'
            }).done(function(){
              toastr.success('Successfully Updated');
              $('#visitorStatusModal').modal('toggle');
							table.ajax.reload();
							$('.iradio_flat').removeClass('checked');
            });
        })
        $('visitorStatusUpdate').on('reset', function(e){
					var form = $(this);
				setTimeout(function() {
						form.find('input').each(function (index, element) {
								$(element).icheck('updated', function(node) {
								});
						});

				});
			});
			$('body').on('click' , '.leave-btn' , function(event){
				var self = $(this);
				var d = new Date().toLocaleTimeString();
				d = d.split(' ');
				var visitorId = self.attr('data-visitor');
				$.ajax({
					url : "{!! route('visitor.leave') !!}",
					method : 'GET',
					data : {id : visitorId ,  departure : d[0] } 
				}).done(function(response){
					if(response)
					{
						swal('Successfully updated', {
							icon : 'success'
						});
						table.ajax.reload();
					}
				});
			});
    });

</script>
@endsection