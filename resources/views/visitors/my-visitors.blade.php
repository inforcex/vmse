@extends('layouts.app') 
@section('page-level-styles')
  <link rel="stylesheet" href="{{ asset('admin/vendor/datatables.net-bs4/dataTables.bootstrap4.min.css') }}"> 
  <link rel="stylesheet" href="{{ asset('admin/vendor/icheck/icheck.min.css') }}"> 
@endsection 
@section('content')
<div class="page">
	<div class="page-content container-fluid">
		<div class="panel">
			<header class="panel-heading">
				<h3 class="panel-title">Visitors</h3>
			</header>
			<div class="panel-body">
				<table class="table table-hover dataTable table-striped w-full" id="visitorsTable">
					<thead>
						<tr>
							<th>Name</th>
							<th>Email</th>
              <th>Status</th>
              <th>Visit Type</th>
							<th>Created</th>
							<th>Actions</th>
						</tr>
					</thead>
					<tbody>
					</tbody>
				</table>
			</div>
		</div>
		<!-- End Panel Table Tools -->
	</div>
</div>

<div class="modal fade" id="visitorStatusModal" aria-labelledby="exampleFormModalLabel" role="dialog" tabindex="-1" aria-hidden="true"
 style="display: none;">
	<div class="modal-dialog modal-simple">
		<form class="modal-content" id="visitorStatusUpdate">
      <input type="hidden" id="visitorId" value="" name="visitorId">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal" aria-label="Close">
					<span aria-hidden="true">×</span>
				</button>
				<h4 class="modal-title" id="exampleFormModalLabel">Set The Status</h4>
			</div>
			<div class="modal-body">
				<div class="row">
					<div class="col-xl-12">
            <ul class="list-group list-group-dividered list-group-full">
              <li class="list-group-item">
                <input type="radio" name="status" value="processing"> Processing
              </li>
              <li class="list-group-item">
                <input type="radio" name="status" value="accepted"> Accepted
              </li>
              <li class="list-group-item">
                <input type="radio" name="status" value="rejected"> Rejected
              </li>
            </ul>
					</div>
					<div class="col-md-12 float-right">
						<button class="btn btn-primary waves-effect waves-classic" type="button" id="visitorStatusBtn">Update Status</button>
					</div>
				</div>
			</div>
		</form>
	</div>
</div>
@endsection 
@section('page-level-scripts')



<script src="{{ asset('admin/vendor/datatables.net/jquery.dataTables.js') }}"></script>
<script src="{{ asset('admin/vendor/datatables.net-bs4/dataTables.bootstrap4.js') }}"></script>
<script src="{{ asset('admin/vendor/datatables.net-buttons/dataTables.buttons.min.js') }}"></script>
<script src="{{ asset('admin/vendor/datatables.net-buttons/buttons.html5.min.js') }}"></script>
<script src="{{ asset('admin/vendor/datatables.net-buttons/buttons.print.min.js') }}"></script>
<script src="{{ asset('admin/vendor/datatables.net-buttons-bs4/buttons.bootstrap4.min.js')}}"></script>
<script src="{{ asset('admin/vendor/icheck/icheck.min.js')}}"></script>
<script>
	$(document).ready(function(){
        $('#visitorStatusModal input').iCheck({
          radioClass: 'iradio_flat'
        });
    		var table = $('#visitorsTable').DataTable({
    			processing : true,
    			serverSide : true,
    			ajax : '{!! route('datatable.myVisitors') !!}',
    			columns: [
		            { data: 'first_name', name: 'first_name' },
		            { data: 'email', name: 'email' },
                { data: 'status', name: 'status' },
								{ data : 'type' , name : 'type'},
		            { data: 'created_at', name: 'created_at' },
		            { data : 'action' , name : 'action' , orderable: false, searchable: false}
		        ],
		        sDom: '<"dt-panelmenu clearfix"Bfr>t<"dt-panelfooter clearfix"ip>',
		        buttons: ['csv', 'print']
    		});
        $('body').on('click' , '.change-status-btn' , function(){
            var visitorId = $(this).attr('data-visitor');
            $('#visitorId').val(visitorId);
        });
        $('body').on('click' , '#visitorStatusBtn' , function(event){
            $.ajax({
              url : '{!! route('visitor.status.update') !!}',
              data : $('#visitorStatusUpdate').serialize(),
              method : 'GET'
            }).done(function(){
              toastr.success('Successfully Updated');
              $('#visitorStatusModal').modal('toggle');
							$('#visitorStatusUpdate').reset();
              table.ajax.reload();
            });
        })
        
    	});
</script>
@endsection