@extends('layouts.app') @section('page-level-styles')
<style>
	p {
		margin-bottom: 0;
	}
</style>
@endsection 
@section('content')
<div class="page">
	<div class="page-content container-fluid">
		<div class="row">
			<div class="col-md-12">
				<div class="panel panel-primary panel-line">
					<div class="panel-heading clearfix">
						<h3 class="panel-title float-left">{{ __('Personal Info') }}</h3>
						<a href="javascript:void(0)" class="avatar avatar-lg float-right" style="width: 130px;margin: 10px;">
								<img src="@if(!$visitor->photo) {{ asset('images/visitors/placeholder.png')}} @else {{ $visitor->photo}} @endif" alt="">
							</a>
					</div>
					<div class="panel-body">
						<div class="row">
							<div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('First Name') }}</label>
								<input type="text" value="{{ $visitor->first_name }}" class="form-control" readonly>
							</div>
                            <div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('Middle Name') }}</label>
								<input type="text" value="{{ $visitor->middle_name }}" class="form-control" readonly>
							</div>
                            <div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('Last Name') }}</label>
								<input type="text" value="{{ $visitor->last_name }}" class="form-control" readonly>
							</div>
						</div>
                        <div class="row">
							<div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('Email') }}</label>
								<input type="text" value="{{ $visitor->email }}" class="form-control" readonly>
							</div>
                            <div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('Phone') }}</label>
								<input type="text" value="{{ $visitor->phone }}" class="form-control" readonly>
							</div>
                            <div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('EID') }}</label>
								<input type="text" value="{{ $visitor->national_id }}" class="form-control" readonly>
							</div>
						</div>
                        <div class="row">
							<div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('Date of Birth') }}</label>
								<input type="text" value="{{ $visitor->dob->format('y/m/d') }}" class="form-control" readonly>
							</div>
                            <div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('Gender') }}</label>
								<input type="text" value="@if($visitor->gender == 'M'){{ 'Male' }}@else{{ 'Female'}} @endif" class="form-control" readonly>
							</div>
                            <div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('Passport Number') }}</label>
								<input type="text" value="{{ $visitor->passport_no }}" class="form-control" readonly>
							</div>
						</div>
                        <div class="row">
							<div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('Nationality') }}</label>
								<input type="text" value="{{ $visitor->nationality }}" class="form-control" readonly>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary panel-line">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ __('Visit Information') }}</h3>
                    </div>
                    <div class="panel-body">
						<div class="row">
							<div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('Visit Date') }}</label>
								<input type="text" value="{{ $visitor->visit_date->format('Y-m-d') }}" class="form-control" readonly>
							</div>
                            <div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('Visit Time') }}</label>
								<input type="text" value="{{ $visitor->visit_time }}" class="form-control" readonly>
							</div>
							<div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('Departure Time') }}</label>
								<input type="text" value="{{ $visitor->departure }}" class="form-control" readonly>
							</div>
						</div>
                        <div class="row">
						<div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('Visit Type') }}</label>
								<input type="text" value="{{ $visitor->visit_type }}" class="form-control" readonly>
							</div>
							<div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('Host') }}</label>
								<input type="text" value="{{ $visitor->user->name }}" class="form-control" readonly>
							</div>
                            <div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('Department') }}</label>
								<input type="text" value="@if($visitor->user->department){{ $visitor->user->department->name }}@endif" class="form-control" readonly>
							</div>
						</div>
						<div class="row">
							<div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('Reason') }}</label>
								<textarea class="form-control" name="" id="" cols="30" rows="7" readonly>{{ $visitor->reason }}</textarea>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
		<div class="row">
            <div class="col-md-12">
                <div class="panel panel-primary panel-line">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{ __('Vehicle Info') }}</h3>
                    </div>
                    <div class="panel-body">
						<div class="row">
							<div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('Emirate Vehicle') }}</label>
								<input type="text" value="{{ $visitor->emirate_vehicle }}" class="form-control" readonly>
							</div>
                            <div class="form-group form-material col-md-4">
								<label class="form-control-label">{{ __('Plate Number') }}</label>
								<input type="text" value="{{ $visitor->plate_number }}" class="form-control" readonly>
							</div>
						</div>
                    </div>
                </div>
            </div>
        </div>
	</div>
</div>
@endsection