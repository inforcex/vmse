@extends('layouts.app')

@section('page-level-styles')
	<link rel="stylesheet" href="{{ asset('css/intlTelInput.css')}}">
	<link rel="stylesheet" href="{{ asset('admin/vendor/select2/select2.min.css') }}">
		<link rel="stylesheet" href="{{ asset('admin/vendor/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">
	<link rel="stylesheet" href="{{ asset('admin/vendor/timepicker/jquery-timepicker.min.css') }}">
@endsection

@section('content')
	
	<div class="page">
		<div class="page-content container">
			<form autocomplete="off" method="POST" action="{{ route('visitors.update' , ['id' => $visitor->id]) }}">
				{{ csrf_field() }}
				{{ method_field('PATCH')}}
				<input type="hidden" name="front" value="0">
				<input type="hidden" name="tenant_id" value="{{ auth()->user()->tenant_id }}">
				<div class="row">
					<div class="col-md-6">
						<div class="panel">
							<div class="panel panel-primary panel-line">
								<div class="panel-heading">
									<h3 class="panel-title">
										{{ __('Personal Info')}}
									</h3>
								</div>
								<div class="panel-body">
									<div class="form-group{{ $errors->has('first_name') ? ' has-danger' : '' }}">
										<label for="name" class="col-md-4 control-label">{{ __('First Name') }}</label>
										<div class="col-md-12">
											<input id="first_name" type="text" class="form-control{{ $errors->has('first_name') ? ' is-invalid' : '' }}" value="{{ $visitor->first_name }}" name="first_name"  required autofocus>

											@if ($errors->has('first_name'))
												<span class="invalid-feedback">
													<strong>{{ $errors->first('first_name') }}</strong>
												</span>
											@endif
										</div>
                            		</div>
									<div class="form-group{{ $errors->has('middle_name') ? ' has-danger' : '' }}">
	                                <label for="middle_name" class="col-md-4 control-label">{{ __('Middle Name') }}</label>
	                                <div class="col-md-12">
	                                    <input id="middle_name" type="text" class="form-control{{ $errors->has('middle_name') ? ' is-invalid' : '' }}" value="{{ $visitor->middle_name }}" name="middle_name">

	                                    @if ($errors->has('middle_name'))
	                                        <span class="invalid-feedback">
	                                            <strong>{{ $errors->first('middle_name') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
                            	</div>
								<div class="form-group{{ $errors->has('last_name') ? ' has-danger' : '' }}">
	                                <label for="last_name" class="col-md-4 control-label">{{ __('Last Name') }}</label>
	                                <div class="col-md-12">
	                                    <input id="last_name" type="text" class="form-control{{ $errors->has('last_name') ? ' is-invalid' : '' }}" name="last_name" value="{{ $visitor->last_name }}"  required autofocus>

	                                    @if ($errors->has('last_name'))
	                                        <span class="invalid-feedback">
	                                            <strong>{{ $errors->first('last_name') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
                            	</div>
	                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
	                                <label for="email" class="col-md-4 control-label">{{ __('Email') }}</label>

	                                <div class="col-md-12">
	                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"  required value="{{ $visitor->email }}">

	                                    @if ($errors->has('email'))
	                                        <div class="invalid-feedback">
	                                            <strong>{{ $errors->first('email') }}</strong>
	                                        </div>
	                                    @endif
	                                </div>
	                            </div>
	                            <div class="form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
	                                <label for="email" class="col-md-4 control-label">{{ __('Phone') }}</label>
	                                <div class="col-md-12">
	                                    <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" required value="{{ $visitor->phone }}">
										<span id="valid-msg" class="hide">✓ {{ __('Valid') }}</span>
										<span id="error-msg" class="hide">Invalid number</span>
	                                    @if ($errors->has('phone'))
	                                        <div class="invalid-feedback" id="phone-invalid-feedback">
	                                            <strong>{{ $errors->first('phone') }}</strong>
	                                        </div>
	                                    @endif
	                                </div>
	                            </div>
								<div class="form-group{{ $errors->has('national_id') ? ' has-danger' : '' }}">
	                                <label for="national_id" class="col-md-4 control-label">{{ __('EID')}}</label>
	                                <div class="col-md-12">
	                                    <input id="national_id" type="text" class="form-control{{ $errors->has('national_id') ? ' is-invalid' : '' }}" name="national_id"  required value="{{ $visitor->national_id }}">

	                                    @if ($errors->has('national_id'))
	                                        <span class="invalid-feedback">
	                                            <strong>{{ $errors->first('national_id') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
                            	</div>
								<div class="form-group{{ $errors->has('gender') ? ' has-danger' : '' }}">
	                                <label for="gender" class="col-md-4 control-label">{{ __('Gender') }}</label>

	                                <div class="col-md-12">
	                                    <select class="form-control{{ $errors->has('gender') ? ' is-invalid' : '' }}" name="gender"  style="width: 100%">
											<option value="">{{ __('Select Gender') }}</option>
											<option value="M" @if($visitor->gender == 'M') {{ 'selected'}} @endif>{{ __('Male') }}</option>
											<option value="F" @if($visitor->gender == 'F') {{ 'selected'}} @endif>{{ __('Female') }}</option>
										</select>
	                                    @if ($errors->has('gender'))
	                                        <div class="invalid-feedback">
	                                            <strong>{{ $errors->first('gender') }}</strong>
	                                        </div>
	                                    @endif
	                                </div>
	                            </div>
								<div class="form-group{{ $errors->has('dob') ? ' has-danger' : '' }}">
	                                <label for="dob" class="col-md-4 control-label">{{ __('Date of Birth')}}</label>
	                                <div class="col-md-12">
	                                    <input id="dob" type="date" class="form-control{{ $errors->has('dob') ? ' is-invalid' : '' }}" name="dob"  value="{{ $visitor->dob->format('Y-m-d') }}">

	                                    @if ($errors->has('dob'))
	                                        <span class="invalid-feedback">
	                                            <strong>{{ $errors->first('dob') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
                            	</div>
								<div class="form-group{{ $errors->has('nationality') ? ' has-danger' : '' }}">
	                                <label for="nationality" class="col-md-4 control-label">{{ __('Nationality') }}</label>
	                                <div class="col-md-12">
	                                    <input id="nationality" type="text" class="form-control{{ $errors->has('nationality') ? ' is-invalid' : '' }}" name="nationality" value="{{ $visitor->nationality }}">

	                                    @if ($errors->has('nationality'))
	                                        <span class="invalid-feedback">
	                                            <strong>{{ $errors->first('nationality') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
                            	</div>
								<div class="form-group{{ $errors->has('passport_no') ? ' has-danger' : '' }}">
	                                <label for="nationality" class="col-md-4 control-label">{{ __('Passport Number') }}</label>
	                                <div class="col-md-12">
	                                    <input id="passport_no" type="text" class="form-control{{ $errors->has('passport_no') ? ' is-invalid' : '' }}" name="passport_no" value="{{ $visitor->passport_no }}">

	                                    @if ($errors->has('passport_no'))
	                                        <span class="invalid-feedback">
	                                            <strong>{{ $errors->first('passport_no') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
                            	</div>
								</div>
							</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="panel panel-primary panel-line">
							<div class="panel-heading">
								<h3 class="panel-title">{{ __('Visit Information') }}</h3>
							</div>
							<div class="panel-body">
								<div class="form-group{{ $errors->has('visit_date') ? ' has-danger' : '' }}">
	                                <label for="visit_date" class="col-md-4 control-label">{{ __('Visit Date')}}</label>
	                                <div class="col-md-12">
	                                    <input id="visit_date" type="date" class="form-control{{ $errors->has('visit_time') ? ' is-invalid' : '' }}" name="visit_date"  value="{{ $visitor->visit_date->format('Y-m-d') }}">

	                                    @if ($errors->has('visit_date'))
	                                        <span class="invalid-feedback">
	                                            <strong>{{ $errors->first('visit_date') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
                            	</div>
								<div class="form-group{{ $errors->has('visit_time') ? ' has-danger' : '' }}">
	                                <label for="visit_time" class="col-md-4 control-label">{{ __('Visit Time')}}</label>
	                                <div class="col-md-12">
	                                    <input id="visit_time" type="text" class="form-control{{ $errors->has('visit_time') ? ' is-invalid' : '' }} timepicker" name="visit_time"  value="{{ $visitor->visit_time }}">

	                                    @if ($errors->has('visit_time'))
	                                        <span class="invalid-feedback">
	                                            <strong>{{ $errors->first('visit_time') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
                            	</div>
								<div class="form-group{{ $errors->has('departure') ? ' has-danger' : '' }}">
	                                <label for="departure" class="col-md-4 control-label">{{ __('Departure Time')}}</label>
	                                <div class="col-md-12">
	                                    <input id="departure" type="text" class="form-control{{ $errors->has('departure') ? ' is-invalid' : '' }} timepicker" name="departure"  value="{{ $visitor->departure }}">

	                                    @if ($errors->has('departure'))
	                                        <span class="invalid-feedback">
	                                            <strong>{{ $errors->first('departure') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
                            	</div>
								<div class="form-group{{ $errors->has('visitor_type') ? ' has-danger' : '' }}">
	                                <label for="visitor_type" class="col-md-4 control-label">{{ __('Visitor Type') }}</label>

	                                <div class="col-md-12">
	                                    <select class="form-control{{ $errors->has('visitor_type') ? ' is-invalid' : '' }}" name="visitor_type">
											<option value="">{{ __('Select Visit Type') }}</option>
											@foreach($visitorTypes as $visitType)
												<option value="{{ $visitType->id }}" @if($visitor->visitor_type == $visitType->id) {{ 'selected' }} @endif>{{ $visitType->name }}</option>
											@endforeach
										</select>
	                                    @if ($errors->has('visitor_type'))
	                                        <div class="invalid-feedback">
	                                            <strong>{{ $errors->first('visitor_type') }}</strong>
	                                        </div>
	                                    @endif
	                                </div>
	                            </div>
								<div class="form-group{{ $errors->has('department_id') ? ' has-danger' : '' }}">
	                                <label for="department_id" class="col-md-4 control-label">{{ __('Department') }}</label>

	                                <div class="col-md-12">
	                                    <select class="form-control{{ $errors->has('department_id') ? ' is-invalid' : '' }}" name="department_id">
											<option value="">{{ __('Select a Department') }}</option>
											@foreach($departments as $department)
												<option value="{{ $department->id }}" @if($visitor->department_id == $department->id) {{ 'selected' }} @endif>{{ $department->name }}</option>
											@endforeach
										</select>
	                                    @if ($errors->has('department_id'))
	                                        <div class="invalid-feedback">
	                                            <strong>{{ $errors->first('department_id') }}</strong>
	                                        </div>
	                                    @endif
	                                </div>
	                            </div>
								<div class="form-group{{ $errors->has('user_id') ? ' has-danger' : '' }}">
	                                <label for="host" class="col-md-4 control-label">{{ __('Host') }}</label>

	                                <div class="col-md-12">
	                                    <select class="form-control{{ $errors->has('user_id') ? ' is-invalid' : '' }}" name="user_id" id="hostSelect" style="width: 100%">
											<option value="{{ $visitor->user_id }}">{{ $visitor->user->name }}</option>
										</select>
	                                    @if ($errors->has('user_id'))
	                                        <div class="invalid-feedback">
	                                            <strong>{{ $errors->first('user_id') }}</strong>
	                                        </div>
	                                    @endif
	                                </div>
	                            </div>
								<div class="form-group{{ $errors->has('reason') ? ' has-danger' : '' }}">
	                                <label for="reason" class="col-md-4 control-label">{{ __('Reason') }}</label>
	                                <div class="col-md-12">
	                                    <textarea name="reason" id="" cols="30" rows="5" class="form-control{{ $errors->has('reason') ? ' is-invalid' : '' }}">{{ $visitor->reason}}</textarea>
	                                    @if ($errors->has('reason'))
	                                        <span class="invalid-feedback">
	                                            <strong>{{ $errors->first('reason') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
                            	</div>
								<h3 class="panel-title">{{ __('Vehicle Info') }}</h3>
								<div class="form-group{{ $errors->has('emirate_vehicle') ? ' has-danger' : '' }}">
	                                <label for="emirate_vehicle" class="col-md-4 control-label">{{ __('Emirate Vehicle')}}</label>
	                                <div class="col-md-12">
	                                    <input id="emirate_vehicle" type="text" class="form-control{{ $errors->has('emirate_vehicle') ? ' is-invalid' : '' }}" name="emirate_vehicle"  value="{{ $visitor->emirate_vehicle }}">
	                                    @if ($errors->has('emirate_vehicle'))
	                                        <span class="invalid-feedback">
	                                            <strong>{{ $errors->first('emirate_vehicle') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
                            	</div>
								<div class="form-group{{ $errors->has('plate_number') ? ' has-danger' : '' }}">
	                                <label for="plate_number" class="col-md-4 control-label">{{ __('Plate Number')}}</label>
	                                <div class="col-md-12">
	                                    <input id="plate_number" type="text" class="form-control{{ $errors->has('plate_number') ? ' is-invalid' : '' }}" name="plate_number"  value="{{ $visitor->plate_number }}">
	                                    @if ($errors->has('plate_number'))
	                                        <span class="invalid-feedback">
	                                            <strong>{{ $errors->first('plate_number') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
                            	</div>
								<div class="form-group">
									<div class="col-md-12">
										<button id="visitorSubmit" type="submit" class="btn btn-primary ladda-button waves-effect waves-light waves-round" data-style="slide-right" data-plugin="ladda">
											<span class="ladda-label">{{ __('Update') }}<i class="icon md-long-arrow-right ml-10" aria-hidden="true"></i></span>
											<span class="ladda-spinner"></span><div class="ladda-progress" style="width: 0px;"></div>
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</form>
		</div>
	</div>

@endsection

@section('page-level-scripts')
	<script src="{{asset('js/intlTelInput.min.js')}}"></script>
	<script src="{{ asset('admin/vendor/select2/select2.full.min.js') }}"></script>
	<script src="{{ asset('admin/js/Plugin/select2.js') }}"></script>
	<script src="{{ asset('admin/vendor/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>
	<script src="{{ asset('admin/vendor/timepicker/jquery.timepicker.min.js') }}"></script>
	<script>
		$(document).ready(function(){
			$('.datepicker').datepicker({
				format : 'yyyy-mm-dd'
			});
			$('.timepicker').timepicker({
				'timeFormat': 'H:i:s'
			});
			  var telInput = $("#phone"),
				
			  errorMsg = $("#error-msg"),
			  validMsg = $("#valid-msg");
				
			// initialise plugin
			telInput.intlTelInput({
				initialCountry: "ae",
			  utilsScript: "{{ asset('js/utils.js')}}"
			});

			var reset = function() {
			  telInput.removeClass("error");
			  errorMsg.addClass("hide");
			  validMsg.addClass("hide");
			};

			// on blur: validate
			telInput.blur(function() {
			  reset();
			  if ($.trim(telInput.val())) {
			    if (telInput.intlTelInput("isValidNumber")) {
			      validMsg.removeClass("hide");
			      $('#visitorSubmit').removeAttr('disabled');
			    } else {
			      telInput.addClass("error");
			      errorMsg.removeClass("hide");
			    }
			  }
			});

			// on keyup / change flag: reset
			telInput.on("keyup change", reset);

			$('#hostSelect').select2({
				ajax : {
					url : '{!! route("employees.search") !!}?tenant={!! auth()->user()->tenant_id !!}',
					dataType : 'json',
					delay: 250,
			    data: function (params) {
			      return {
			        q: params.term
			      };
			    },
			    processResults: function (data, params) {
			      // parse the results into the format expected by Select2
			      // since we are using custom formatting functions we do not need to
			      // alter the remote JSON data, except to indicate that infinite
			      // scrolling can be used
			      var data1 = $.map(data , function(obj){

                            obj.text = obj.text || obj.name
                            return obj;

                        });
			      console.log(data1);
			      return {
                            results : data1
                        };
			    },
			    cache: true
			  },
			  placeholder: 'Search Host...',
			  minimumInputLength: 3
			});
		});
	</script>
@endsection