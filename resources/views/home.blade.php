@extends('layouts.app') @section('page-level-styles')

<link rel="stylesheet" href="{{ asset('global/vendor/chartist/chartist.min.css') }}">
<link rel="stylesheet" href="{{ asset('global/vendor/jvectormap/jquery-jvectormap.min.css') }}">
<link rel="stylesheet" href="{{ asset('global/vendor/chartist-plugin-tooltip/chartist-plugin-tooltip.min.css') }}">
<link rel="stylesheet" href="{{ asset('css/dashboard/v1.min.css') }}">
@endsection
@section('content')
<!-- Page -->
<div class="page">
	<div class="page-content container-fluid">
    @if(auth()->user()->hasRole(['admin' , 'super_admin']))
		<div class="row" data-plugin="matchHeight" data-by-row="true">
		<div class="col-xl-3 col-md-6">
				<!-- Widget Linearea One-->
				<div class="card card-shadow" id="widgetLineareaOne">
					<div class="card-block p-20 pt-10">
						<div class="clearfix">
							<div class="grey-800 float-left py-10">
								{{ __('Checked in')}} <img class="custom-icon" src="{{ asset('admin/images/visitors.png')}}">
							</div>
							<span class="float-right grey-700 font-size-30">{{ $signedInVisitors }}</span>
						</div>
					</div>
				</div>
				<!-- End Widget Linearea One -->
			</div>
			<div class="col-xl-3 col-md-6">
				<!-- Widget Linearea One-->
				<div class="card card-shadow" id="widgetLineareaOne">
					<div class="card-block p-20 pt-10">
						<div class="clearfix">
							<div class="grey-800 float-left py-10">
								{{ __('Checked Out')}} <img class="custom-icon" src="{{ asset('admin/images/visitors.png')}}">
							</div>
							<span class="float-right grey-700 font-size-30">{{ $signedOutVisitors }}</span>
						</div>
					</div>
				</div>
				<!-- End Widget Linearea One -->
			</div>
			<div class="col-xl-3 col-md-6">
				<!-- Widget Linearea One-->
				<div class="card card-shadow" id="widgetLineareaOne">
					<div class="card-block p-20 pt-10">
						<div class="clearfix">
							<div class="grey-800 float-left py-10">
								{{ __('Expected')}} <img class="custom-icon" src="{{ asset('admin/images/visitors.png')}}">
							</div>
							<span class="float-right grey-700 font-size-30">{{ $expectedVisitors }}</span>
						</div>
					</div>
				</div>
				<!-- End Widget Linearea One -->
			</div>
			<div class="col-xl-3 col-md-6">
				<!-- Widget Linearea One-->
				<div class="card card-shadow" id="widgetLineareaOne">
					<div class="card-block p-20 pt-10">
						<div class="clearfix">
							<div class="grey-800 float-left py-10">
								{{ __('Today Visits')}} <img class="custom-icon" src="{{ asset('admin/images/visitors.png')}}">
							</div>
							<span class="float-right grey-700 font-size-30">{{ $todaysVisitors }}</span>
						</div>
					</div>
				</div>
				<!-- End Widget Linearea One -->
			</div>
			<div class="col-xl-3 col-md-6">
				<!-- Widget Linearea One-->
				<div class="card card-shadow" id="widgetLineareaOne">
					<div class="card-block p-20 pt-10">
						<div class="clearfix">
							<div class="grey-800 float-left py-10">
								{{ __('Month Visits') }} <img class="custom-icon" src="{{ asset('admin/images/visitors.png')}}">
							</div>
							<span class="float-right grey-700 font-size-30">{{ $thisMonthVisitors }}</span>
						</div>
					</div>
				</div>
				<!-- End Widget Linearea One -->
			</div>
			<div class="col-xl-3 col-md-6">
				<!-- Widget Linearea One-->
				<div class="card card-shadow" id="widgetLineareaOne">
					<div class="card-block p-20 pt-10">
						<div class="clearfix">
							<div class="grey-800 float-left py-10">
								{{ __('Visitors Requests')}} <img class="custom-icon" src="{{ asset('admin/images/visitors.png')}}">
							</div>
							<span class="float-right grey-700 font-size-30">{{ $processingVisitors }}</span>
						</div>
					</div>
				</div>
				<!-- End Widget Linearea One -->
			</div>
			<div class="col-xl-3 col-md-6">
				<!-- Widget Linearea One-->
				<div class="card card-shadow" id="widgetLineareaOne">
					<div class="card-block p-20 pt-10">
						<div class="clearfix">
							<div class="grey-800 float-left py-10">
								{{ __('Accepted Visits') }} <img class="custom-icon" src="{{ asset('admin/images/visitors.png')}}">
							</div>
							<span class="float-right grey-700 font-size-30">{{ $acceptedVisitors }}</span>
						</div>
					</div>
				</div>
				<!-- End Widget Linearea One -->
			</div>
			{{--  <div class="col-xl-3 col-md-6">
				<!-- Widget Linearea One-->
				<div class="card card-shadow" id="widgetLineareaOne">
					<div class="card-block p-20 pt-10">
						<div class="clearfix">
							<div class="grey-800 float-left py-10">
								Rejected Visits<i class="icon md-account grey-600 font-size-24 vertical-align-bottom mr-5"></i>
							</div>
							<span class="float-right grey-700 font-size-30">{{ $rejectedVisitors }}</span>
						</div>
					</div>
				</div>
				<!-- End Widget Linearea One -->
			</div>  --}}
			<div class="col-xl-3 col-md-6">
				<!-- Widget Linearea Two -->
				<div class="card card-shadow" id="widgetLineareaTwo">
					<div class="card-block p-20 pt-10">
						<div class="clearfix">
							<div class="grey-800 float-left py-10">
								{{ __('All Visits') }} <img class="custom-icon" src="{{ asset('admin/images/visitors.png')}}">
							</div>
							<span class="float-right grey-700 font-size-30">{{ $visitors }}</span>
						</div>
					</div>
				</div>
				<!-- End Widget Linearea Two -->
			</div>
			<div class="col-xl-3 col-md-6">
				<!-- Widget Linearea One-->
				<div class="card card-shadow" id="widgetLineareaOne">
					<div class="card-block p-20 pt-10">
						<div class="clearfix">
							<div class="grey-800 float-left py-10">
								{{ __('Employees') }} <img class="custom-icon" src="{{ asset('admin/images/employee.png')}}">
							</div>
							<span class="float-right grey-700 font-size-30">{{ $employees }}</span>
						</div>
					</div>
				</div>
				<!-- End Widget Linearea One -->
			</div>
			<div class="col-xl-3 col-md-6">
				<!-- Widget Linearea Three -->
				<div class="card card-shadow" id="widgetLineareaThree">
					<div class="card-block p-20 pt-10">
						<div class="clearfix">
							<div class="grey-800 float-left py-10">
								{{ __('Admins') }} <img class="custom-icon" src="{{ asset('admin/images/admin.png')}}">
							</div>
							<span class="float-right grey-700 font-size-30">{{ $admins }}</span>
						</div>
					</div>
				</div>
				<!-- End Widget Linearea Three -->
			</div>
			<div class="col-xl-3 col-md-6">
				<!-- Widget Linearea Four -->
				<div class="card card-shadow" id="widgetLineareaFour">
					<div class="card-block p-20 pt-10">
						<div class="clearfix">
							<div class="grey-800 float-left py-10">
								{{ __('Departments') }} <img class="custom-icon" src="{{ asset('admin/images/sections.png')}}">
							</div>
							<span class="float-right grey-700 font-size-30">{{ $departments }}</span>
						</div>
					</div>
				</div>
				<!-- End Widget Linearea Four -->
			</div> 
		</div>
        @endif
        @if(auth()->user()->hasRole('employee'))
		<div class="row" data-plugin="matchHeight" data-by-row="true">
			<div class="col-md-12">
				<h3 class="panel-title pl-0">Stats for : {{ auth()->user()->name }}</h3>
			</div>

			<div class="col-xl-3 col-md-6">
				<!-- Widget Linearea One-->
				<div class="card card-shadow" id="widgetLineareaOne">
					<div class="card-block p-20 pt-10">
						<div class="clearfix">
							<div class="grey-800 float-left py-10">
								{{ __('Today Visits')}} <img class="custom-icon" src="{{ asset('admin/images/visitors.png')}}">
							</div>
							<span class="float-right grey-700 font-size-30">{{ $employeeTodayVisitors }}</span>
						</div>
					</div>
				</div>
			</div>
			<!-- End Widget Linearea One -->
			<div class="col-xl-3 col-md-6">
				<!-- Widget Linearea One-->
				<div class="card card-shadow" id="widgetLineareaOne">
					<div class="card-block p-20 pt-10">
						<div class="clearfix">
							<div class="grey-800 float-left py-10">
								{{ __('Visits Under Review')}} <img class="custom-icon" src="{{ asset('admin/images/visitors.png')}}">
							</div>
							<span class="float-right grey-700 font-size-30">{{ $employeeProcessingVisitors }}</span>
						</div>
					</div>
				</div>
				<!-- End Widget Linearea One -->
			</div>
			<div class="col-xl-3 col-md-6">
				<!-- Widget Linearea One-->
				<div class="card card-shadow" id="widgetLineareaOne">
					<div class="card-block p-20 pt-10">
						<div class="clearfix">
							<div class="grey-800 float-left py-10">
								{{ __('Accepted Visits') }} <img class="custom-icon" src="{{ asset('admin/images/visitors.png')}}">
							</div>
							<span class="float-right grey-700 font-size-30">{{ $employeeAcceptedVisitors }}</span>
						</div>
					</div>
				</div>
				<!-- End Widget Linearea One -->
			</div>
			{{--  <div class="col-xl-3 col-md-6">
				<!-- Widget Linearea One-->
				<div class="card card-shadow" id="widgetLineareaOne">
					<div class="card-block p-20 pt-10">
						<div class="clearfix">
							<div class="grey-800 float-left py-10">
								<i class="icon md-account grey-600 font-size-24 vertical-align-bottom mr-5"></i>Rejected Visits
							</div>
							<span class="float-right grey-700 font-size-30">{{ $employeeRejectedVisitors }}</span>
						</div>
					</div>
				</div>
				<!-- End Widget Linearea One -->
			</div>  --}}
            <div class="col-xl-3 col-md-6">
				<!-- Widget Linearea One-->
				<div class="card card-shadow" id="widgetLineareaOne">
					<div class="card-block p-20 pt-10">
						<div class="clearfix">
							<div class="grey-800 float-left py-10">
								{{ __('All Visits') }} <img class="custom-icon" src="{{ asset('admin/images/visitors.png')}}">
							</div>
							<span class="float-right grey-700 font-size-30">{{ $employeeAllVisitors }}</span>
						</div>
					</div>
				</div>
				<!-- End Widget Linearea One -->
			</div>
		</div>
        @endif
	<div class="row">
		<div class="col-md-12">
			<div class="card card-shadow">
				<div class="card-block p-20 pt-10">
					{{ __('Public Visitors Link')}}
					<pre>{{route('visitors.front' , $tenant->identifier)}}</pre>
				</div>
			</div>
		</div>
	</div>
</div>
</div>
<!-- End Page -->
@endsection