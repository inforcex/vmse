@extends('layouts.app')

@section('page-level-styles')
	<link rel="stylesheet" href="{{ asset('css/intlTelInput.css')}}">
@endsection

@section('content')
	
	<div class="page">
		<div class="page-content container">
			<div class="row">
				<div class="col-md-6">
					<div class="panel panel-primary panel-line">
						<div class="panel-heading">
							<h3 class="panel-title">
								{{ __('Add Employee') }}
							</h3>
						</div>
						<div class="panel-body">
							<form autocomplete="off" method="POST" action="{{ route('employees.store') }}">
								{{ csrf_field() }}
	                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
	                                <label for="name" class="col-md-4 control-label">{{ __('Name') }}</label>
	                                <div class="col-md-12">
	                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" value="{{ old('name') }}" name="name"  required autofocus>

	                                    @if ($errors->has('name'))
	                                        <span class="invalid-feedback">
	                                            <strong>{{ $errors->first('name') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
                            	</div>
	                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
	                                <label for="email" class="col-md-4 control-label">{{ __('Email') }}</label>

	                                <div class="col-md-12">
	                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" value="{{ old('email') }}" name="email"  required>

	                                    @if ($errors->has('email'))
	                                        <div class="invalid-feedback">
	                                            <strong>{{ $errors->first('email') }}</strong>
	                                        </div>
	                                    @endif
	                                </div>
	                            </div>
								<div class="form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
	                                <label for="email" class="col-md-4 control-label">{{ __('Phone')}}</label>

	                                <div class="col-md-12">
	                                    <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" value="{{ old('phone') }}" required>
										<input type="hidden" name="phone" value="" id="hiddenPhone">
										<span id="valid-msg" class="hide">✓ {{ __('Valid') }}</span>
										<span id="error-msg" class="hide">{{ __('Invalid') }}</span>
	                                    @if ($errors->has('phone'))
	                                        <div class="invalid-feedback" id="phone-invalid-feedback">
	                                            <strong>{{ $errors->first('phone') }}</strong>
	                                        </div>
	                                    @endif
	                                </div>
	                            </div>
								<div class="form-group{{ $errors->has('department') ? ' has-danger' : '' }}">
	                                <label for="email" class="col-md-4 control-label">{{ __('Department') }}</label>

	                                <div class="col-md-12">
	                                    <select name="department" class="form-control">
											<option value="">{{ __('Select a Department')}}</option>
											@foreach($departments as $department)
												<option value="{{$department->id}}">{{ $department->name }}</option>
											@endforeach
										</select>

	                                    @if ($errors->has('department'))
	                                        <div class="invalid-feedback">
	                                            <strong>{{ $errors->first('department') }}</strong>
	                                        </div>
	                                    @endif
	                                </div>
	                            </div>
								@include('partials._btn-create')
							</form>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>

@endsection

@section('page-level-scripts')
	<script src="{{asset('js/intlTelInput.min.js')}}"></script>
	<script>
		$(document).ready(function(){
			  var telInput = $("#phone"),
			  errorMsg = $("#error-msg"),
			  validMsg = $("#valid-msg");

			// initialise plugin
			telInput.intlTelInput({
			  utilsScript: "{{ asset('js/utils.js')}}"
			});

			var reset = function() {
			  telInput.removeClass("error");
			  errorMsg.addClass("hide");
			  validMsg.addClass("hide");
			};

			// on blur: validate
			telInput.blur(function() {
			  reset();
			  if ($.trim(telInput.val())) {
			    if (telInput.intlTelInput("isValidNumber")) {
			      validMsg.removeClass("hide");
				  $('#hiddenPhone').val($("#phone").intlTelInput("getNumber"));
			      $('#adminSubmit').removeAttr('disabled');
			    } else {
			      telInput.addClass("error");
			      errorMsg.removeClass("hide");
			    }
			  }
			});

			// on keyup / change flag: reset
			telInput.on("keyup change", reset);
		});
	</script>
@endsection