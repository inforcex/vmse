@extends('layouts.app')
@section('page-level-styles')

	<link rel="stylesheet" href="{{ asset('css/dropzone.min.css')}}">
	<link rel="stylesheet" href="{{ asset('css/dropzone-basic.min.css')}}">
	<link rel="stylesheet" href="{{ asset('css/intlTelInput.css')}}">
	<style>
		.dropzone{
			border : 2px dashed #0087F7;
		}
	</style>
@endsection
@section('content')
	<div class="page">
		<div class="page-content container">
			<div class="row">
				<div class="col-lg-3">
					<div class="card card-shadow text-center">
						<div class="card-block">
							<a href="javascript:void(0)" class="avatar avatar-lg" style="width: 130px;margin-bottom: 10px;">
								<img src="@if(!auth()->user()->avatar_id) {{ asset('images/avatars/placeholder.png')}} @else {{ auth()->user()->avatar->path()}} @endif" alt="">
							</a>
							<h4 class="profile-user">
								{{ $user->name }}
							</h4>
							<p class="profile-job">
								@foreach($user->roles as $role)
									{{ $role->description }}
								@endforeach
							</p>
						</div>
					</div>
				</div>
				<div class="col-lg-9">
					<div class="panel">
						<div class="panel-heading"><h3 class="panel-title">{{ __('Personal Info') }}</h3></div>
						<div class="panel-body">
							<form autocomplete="off" method="POST" action="{{ route('employees.update' , $user->id) }}">
								{{ csrf_field() }}
								{{ method_field('PUT') }}
	                            <div class="form-group{{ $errors->has('name') ? ' has-danger' : '' }}">
	                                <label for="name" class="col-md-4 control-label">{{ __('Name') }}</label>
	                                <div class="col-md-12">
	                                    <input id="name" type="text" class="form-control{{ $errors->has('name') ? ' is-invalid' : '' }}" name="name"  required autofocus value="{{ $user->name }}">

	                                    @if ($errors->has('name'))
	                                        <span class="invalid-feedback">
	                                            <strong>{{ $errors->first('name') }}</strong>
	                                        </span>
	                                    @endif
	                                </div>
                            	</div>
	                            <div class="form-group{{ $errors->has('email') ? ' has-danger' : '' }}">
	                                <label for="email" class="col-md-4 control-label">{{ __('Email') }}</label>

	                                <div class="col-md-12">
	                                    <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}"" name="email"  required value="{{ $user->email }}">

	                                    @if ($errors->has('email'))
	                                        <div class="invalid-feedback">
	                                            <strong>{{ $errors->first('email') }}</strong>
	                                        </div>
	                                    @endif
	                                </div>
	                            </div>
	                            <div class="form-group{{ $errors->has('phone') ? ' has-danger' : '' }}">
	                                <label for="email" class="col-md-4 control-label">{{ __('Phone') }}</label>

	                                <div class="col-md-12">
	                                    <input id="phone" type="text" class="form-control{{ $errors->has('phone') ? ' is-invalid' : '' }}" name="phone" required value="{{ $user->phone }}">
										<span id="valid-msg" class="hide">✓ {{ __('Valid') }}</span>
										<span id="error-msg" class="hide">{{ __('Invalid') }}</span>
	                                    @if ($errors->has('phone'))
	                                        <div class="invalid-feedback" id="phone-invalid-feedback">
	                                            <strong>{{ $errors->first('phone') }}</strong>
	                                        </div>
	                                    @endif
	                                </div>
	                            </div>
								<div class="form-group col-md-12">
									<button id="adminSubmit" type="submit" class="btn btn-primary ladda-button waves-effect waves-light waves-round" data-style="slide-right" data-plugin="ladda">
                        			<span class="ladda-label">{{ __('Update') }}<i class="icon md-long-arrow-right ml-10" aria-hidden="true"></i></span>
                      				<span class="ladda-spinner"></span><div class="ladda-progress" style="width: 0px;"></div>
                      			</button>
								</div>
							</form>	
						</div>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-lg-3"></div>
				<div class="col-lg-9">
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">{{ __('Change Avatar') }}</h3>
						</div>
						<div class="panel-body">
							<div id="dropzone">
								<form action="{{ route('avatar.upload') }}" method="POST" class="dropzone" id="my-awesome-dropzone">
									{{ csrf_field() }}
								</form>
								
							</div>
							<form action="{{ route('avatar.update')}}" method="POST">
								{{ csrf_field()}}
								<input type="hidden" name="avatar_id" value="" id="avatar_id">
								<button type="submit" class="btn btn-primary" disabled id="updateAvatarBtn">{{ __('Update Avatar') }}</button>
							</form>
						</div>
					</div>
				</div>
			</div>
			@if(auth()->user()->hasRole('super_admin'))
			<div class="row">
				<div class="col-lg-3"></div>
				<div class="col-lg-9">
					<div class="panel">
						<div class="panel-heading">
							<h3 class="panel-title">{{ __('Change Welcome Message') }}</h3>
						</div>
						<div class="panel-body">
							<form action="{{ route('welcome.message.update')}}" method="POST">
								{{ csrf_field()}}
								<div class="form-group{{ $errors->has('welcome_message') ? ' has-danger' : '' }}">
	                                <div class="col-md-12">
	                                    <textarea id="welcome_message" class="form-control{{ $errors->has('welcome_message') ? ' is-invalid' : '' }}" name="welcome_message"  required value="" maxlength="250">{{ auth()->user()->welcome_message}}</textarea> 

	                                    @if ($errors->has('welcome_message'))
	                                        <div class="invalid-feedback">
	                                            <strong>{{ $errors->first('welcome_message') }}</strong>
	                                        </div>
	                                    @endif
	                                </div>
	                            </div>
	                            <div class="form-group col-md-12">
	                            	<button type="submit" class="btn btn-primary" disabled id="updateWelcomeMessage">{{ __('Update Welcome Message') }}</button>
	                            </div>
							</form>
						</div>
					</div>
				</div>
			</div>
			@endif
		</div>
	</div>
@endsection

@section('page-level-scripts')
<script src="{{asset('js/intlTelInput.min.js')}}"></script>
	<script src="{{ asset('js/dropzone.min.js')}}"></script>
	<script>
			var telInput = $("#phone"),
			  errorMsg = $("#error-msg"),
			  validMsg = $("#valid-msg");

			// initialise plugin
			telInput.intlTelInput({
			  utilsScript: "{{ asset('js/utils.js')}}"
			});

			var reset = function() {
			  telInput.removeClass("error");
			  errorMsg.addClass("hide");
			  validMsg.addClass("hide");
			};

			// on blur: validate
			telInput.blur(function() {
			  reset();
			  if ($.trim(telInput.val())) {
			    if (telInput.intlTelInput("isValidNumber")) {
			      validMsg.removeClass("hide");
				  $('#hiddenPhone').val($("#phone").intlTelInput("getNumber"));
			      $('#adminSubmit').removeAttr('disabled');
			    } else {
			      telInput.addClass("error");
			      errorMsg.removeClass("hide");
			    }
			  }
			});

			// on keyup / change flag: reset
			telInput.on("keyup change", reset);
		Dropzone.options.myAwesomeDropzone = {
		  paramName: "avatar", // The name that will be used to transfer the file
		  maxFilesize: 1, // MB
		  addRemoveLinks : true,
		  maxFiles : 1,
		  acceptedFiles : "image/jpeg,image/png,image/gif",
		  success : function(file, response)
		  {
		  	$('#avatar_id').val(response.data.id);
		  	$('#updateAvatarBtn').removeAttr('disabled');
		  }
		};
		$(document).ready(function(){
			$('textarea[maxlength]').on('keyup blur', function() {
				$('#updateWelcomeMessage').removeAttr('disabled');
			    // Store the maxlength and value of the field.
			    var maxlength = $(this).attr('maxlength');
			    var val = $(this).val();

			    // Trim the field if it has content over the maxlength.
			    if (val.length > maxlength) {
			        $(this).val(val.slice(0, maxlength));
			    }
			});
		});

	</script>
@endsection