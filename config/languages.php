<?php

return [
    'en' => [
        'name' => 'English',
        'icon' => 'gb'
    ],
    'ar' => [
        'name' => 'Arabic',
        'icon' => 'ae'
    ]
];