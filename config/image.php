<?php

return [

	'path' => [

		'absolute' => public_path($relative = 'images/avatars'),
		'relative' => $relative,
	]

];