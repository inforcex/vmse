$(document).ready(function(){
    $('body').on('click' , '.delete-submit' , function(event){
        event.preventDefault();
        var form = $(this).parents('.delete-form');
        swal({
            title: "Delete Record?",
            text: "This action is irreversible!",
            icon: "warning",
            buttons: true,
            dangerMode: true,
          })
          .then((willDelete) => {
            if (willDelete) {
                form.submit();
            }
          });
    })
});