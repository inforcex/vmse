<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'name' => 'super_admin',
            'display_name' => 'Super Admin',
            'description' => 'Company',
        ]);

        DB::table('roles')->insert([
            'name' => 'admin',
            'display_name' => 'Admin',
            'description' => 'Admin, Employee',
        ]);

        DB::table('roles')->insert([
            'name' => 'employee',
            'display_name' => 'Employee',
            'description' => 'Employee',
        ]);
    }
}
