<?php

use Illuminate\Database\Seeder;
use App\VisitorType;


class VisitorTypeSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        VisitorType::create([
            'name' => 'Visitor'
        ]);        
    }
}
