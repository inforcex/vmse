<?php

use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('permissions')->insert([
            'name' => 'create_admin',
            'display_name' => 'Create Admin',
            'description' => 'Super Admin can create Admin',
        ]);

        DB::table('permissions')->insert([
            'name' => 'create_employee',
            'display_name' => 'Create Employee',
            'description' => 'Super Admin and Admin can create Employees',
        ]);

        DB::table('permissions')->insert([
            'name' => 'create_department',
            'display_name' => 'Create Department',
            'description' => 'Super Admin and Admin can create Departments',
        ]);

    }
}
