<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateVisitorsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('visitors', function (Blueprint $table) {
            $table->increments('id');
            $table->string('first_name');
            $table->string('middle_name')->nullable();
            $table->string('last_name');
            $table->string('email');
            $table->string('phone');
            $table->string('national_id')->nullable();
            $table->string('passport_no')->nullable();
            $table->string('gender')->nullable();
            $table->string('company')->nullable();
            $table->string('nationality')->nullable();
            $table->date('dob')->nullable();
            $table->integer('visitor_type');
            $table->integer('avatar_id')->nullable();
            $table->string('status')->default('processing');
            $table->date('visit_date')->nullable();
            $table->time('visit_time')->nullable();
            $table->time('departure')->nullable();
            $table->integer('department_id')->nullable();
            $table->string('emirate_vehicle')->nullable();
            $table->string('plate_number')->nullable();
            $table->text('reason')->nullable();
            $table->boolean('front')->nullable();
            $table->integer('user_id');
            $table->integer('tenant_id');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('visitors');
    }
}
