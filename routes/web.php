<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();



Route::get('front/{identifier}' , 'Visitors\VisitorFrontController@create')->name('visitors.front');
Route::get('employees/search' , 'Employees\EmployeeSearchController@show')->name('employees.search');
Route::resource('visitors', 'VisitorsController');
Route::group(['middleware' => ['auth','tenant']] , function(){
	Route::get('/home', 'HomeController@index')->name('home');
	Route::get('/reports', 'ReportsController@index')->name('reports');	
	Route::get('settings' , 'SettingsController@index')->name('settings.index');
	Route::post('welcome_message/update' , 'SettingsController@updateWelcomeMessage')->name('welcome.message.update');
	Route::post('avatar/upload' , 'AvatarController@store')->name('avatar.upload');
	Route::post('avatar/update' , 'AvatarController@updateAvatar')->name('avatar.update');
	Route::get('visitor/status/update' , 'Visitors\VisitorStatusController@update')->name('visitor.status.update');
	Route::get('my/visitors' , 'Visitors\MyVisitorsController@index')->name('my.visitors');
	Route::resource('admins', 'AdminsController');
	Route::resource('employees', 'EmployeesController');
	Route::resource('departments' , 'DepartmentsController');
	Route::get('lang/{lang}' , 'LanguageController@switchLang')->name('lang.switch');
	Route::get('visitors/by/date' , 'ReportsController@visitorsByDate')->name('visitors.by.date');
	Route::get('visitor/leave' , 'Visitors\VisitorLeaveController@create')->name('visitor.leave');
	Route::group(['prefix' => 'datatable'] , function(){

		Route::get('admins' , 'DataTablesController@admins')->name('datatable.admins');
		Route::get('employees' , 'DataTablesController@employees')->name('datatable.employees');
		Route::get('departments' , 'DataTablesController@departments')->name('datatable.departments');
		Route::get('visitors' , 'DataTablesController@visitors')->name('datatable.visitors');
		Route::get('my_visitors' , 'DataTablesController@myVisitors')->name('datatable.myVisitors');


	});
});


